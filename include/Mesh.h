/*
 * \file   Mesh.h
 * \author L. Nagy
 * 
 * MIT License
 *
 * Copyright (c) [2016] Lesleis Nagy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MESH_H_
#define MESH_H_

#include <algorithm>
#include <array>
#include <cmath>
#include <set>
#include <utility>
#include <vector>
#include <map>

#include "BasicTypes.h"

class ReadTecplot;

class Mesh
{
public:
  using Vertex = std::array<Float, 3>;
  using Face = std::array<Index, 4>;
  using Element = std::array<Index, 4>;

  using VertexList = std::vector<Vertex>;
  using VertexNeighbourList = std::vector< std::set<Index> >;

  using FaceElementList = std::map<Face, std::set<Index> >;

  using ElementList = std::vector<Element>;
  using FaceElementNeighbourList = std::vector< std::array<Index, 4> >;

  Mesh() {}

  Mesh(Size nvert, Size nelem) 
  {
    m_verts.resize(nvert);
    m_elems.resize(nelem);
  }

  inline Size nvert() const { return m_verts.size(); }
  inline Size nelem() const { return m_elems.size(); }
  inline const Vertex & vertex(Size idx) const { return m_verts[idx]; }
  inline const Element & element(Size idx) const { return m_elems[idx]; }
  inline const VertexList & vertices() const { return m_verts; };
  inline const ElementList & elements() const { return m_elems; }

  Float volume() const;

  Float esvd() const;

private:
  VertexList m_verts;
  ElementList m_elems;
  VertexNeighbourList m_v_neighbours;
  FaceElementNeighbourList m_fe_neighbours;

  /**
   * Insert a vertex in to the mesh.
   * \param x the x coordinate of the vertex.
   * \param y the y coordinate of the vertex.
   * \param z the z coordinate of the vertex.
   *
   * \return none
   */
  inline void insert_vertex(Float x, Float y, Float z) 
  { 
    m_verts.push_back({x, y, z});
  }

  /**
   * Insert an element (consisting of 4 indices of the vertices that comprise
   * a tetraheral element).
   * \param n0           the index of the first vertex.
   * \param n1           the index of the second vertex.
   * \param n2           the index of the third vertex.
   * \param n3           the index of the fourth vertex.
   * \param zero_correct true if zero correction should be applied to indices
   *                     (by default this is false).
   */
  inline void insert_element(
    Index n0, 
    Index n1, 
    Index n2, 
    Index n3, 
    bool zero_correct=false
  ) {
    if (zero_correct) {
      m_elems.push_back({n0-1, n1-1, n2-1, n3-1});
    } else {
      m_elems.push_back({n0, n1, n2, n3});
    }
  }

  /**
   * Set the value of the vertex at a given position in the vertex list.
   * \param idx the index of the vertex to set/change.
   * \param x the x coordinate of the vertex.
   * \param y the y coordinate of the vertex.
   * \param z the z coordinate of the vertex.
   */
  inline void set_vertex(Index idx, Float x, Float y, Float z) 
  {
    m_verts[idx] = {x, y, z};
  }

  /**
   * Set the value of an element (4 vertex indicies) at a given position in the
   * element list.
   * \param idx the index of the element to set/change.
   * \param n0           the index of the first vertex.
   * \param n1           the index of the second vertex.
   * \param n2           the index of the third vertex.
   * \param n3           the index of the fourth vertex.
   * \param zero_correct true if zero correction should be applied to indices
   *                     (by default this is false).
   */
  inline void set_element(
     Size idx, 
     Index n0, 
     Index n1, 
     Index n2, 
     Index n3, 
     bool zero_correct=false
  ) {
    if (zero_correct) {
      m_elems[idx] = {n0-1, n1-1, n2-1, n3-1};
    } else {
      m_elems[idx] = {n0, n1, n2, n3};
    }
  }

  /**
   * Clear the vertex and element lists.
   */
  inline void clear()
  {
    m_verts.clear();
    m_elems.clear();
  }

  /**
   * Compute the vertex to vertex connectivity of the mesh.
   */
  void v2v_conn();

  /**
   * Compute the element to element connectivity of the mesh by face.
   */
  void e2e_conn_f();

  /**
   * Compute the volume of a tetrahedron given the coordinates of the four 
   * corners.
   */
  Float tetra_volume (
    Float x1, Float y1, Float z1,
    Float x2, Float y2, Float z2,
    Float x3, Float y3, Float z3,
    Float x4, Float y4, Float z4
  ) const ;

  /**
   * Compute the mid-point of a tetrahedron given the coordinates of the four
   * corners.
   */
  Vertex tetra_middle (
    Float x1, Float y1, Float z1,
    Float x2, Float y2, Float z2,
    Float x3, Float y3, Float z3,
    Float x4, Float y4, Float z4
  ) const;


  /**
   * Comparator, will swap i1 and i2 if i1 is greater than i2
   */
  inline void comparator(Index & e1, Index & e2) const {
      using std::swap;

      if (e2 < e1) {
          swap(e1, e2);
      }
  }
      

  /**
   * Return an element sorted by indices.
   */
  inline Element sort(const Element & e) const {
    using std::max;

    Index n0 = e[0];
    Index n1 = e[1];
    Index n2 = e[2];
    Index n3 = e[3];

    comparator(n0, n2);
    comparator(n1, n3); 

    comparator(n0, n1);
    comparator(n2, n3);

    comparator(n1, n2);

    return {n0, n1, n2, n3};
  }

  friend class ReadTecplot;
};

#endif // MESH_H_
