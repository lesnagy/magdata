/**
 * \file   Integrate.h
 * \author L. Nagy
 * 
 * MIT License
 *
 * Copyright (c) [2016] Lesleis Nagy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#ifndef INTEGRATE_H_
#define INTEGRATE_H_

#include <cmath>

#include "BasicTypes.h"
#include "Mesh.h"
#include "ScalarField.h"
#include "VectorField.h"

class Integrate
{
public:
  Float operator () (
    const        Mesh & mesh, 
    const ScalarField & sf
  ) const;
  
  VectorField::Vector operator () (
    const        Mesh & mesh, 
    const VectorField & vf
  ) const;

private:
  Float integrate (
    Float x1, Float y1, Float z1, Float s1,
    Float x2, Float y2, Float z2, Float s2,
    Float x3, Float y3, Float z3, Float s3,
    Float x4, Float y4, Float z4, Float s4
  ) const;
};

#endif // INTEGRATE_H_
