/**
 * \file   Energies.h
 * \author L. Nagy
 * 
 * MIT License
 *
 * Copyright (c) [2016] Lesleis Nagy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#ifndef ENERGIES_H_
#define ENERGIES_H_

#include <array>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>


#include <boost/asio.hpp>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/regex.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#include "legacy_directory_utilities.h"

struct ModelEnergies
{
    double anis;
    double ext;
    double demag;
    double exch1;
    double exch2;
    double exch3;
    double exch4;
    double tot;
};

class Energies
{
public:
    Energies() : m_merrill_exe("merrill") 
    {
        setup_regexs();
    }

    Energies(const std::string merrill_exe): m_merrill_exe(merrill_exe) 
    {
        setup_regexs();
    }

    ModelEnergies operator () (
        const std::string & root,
        const std::string & material,
        const std::string & geometry,
        unsigned int size,
        unsigned int temperature,
        unsigned int index,
        const std::string & uuid_value=""
    );

private:
    void setup_regexs();

    std::vector<std::string> split(
        const std::string & s, 
        char delim
    ); 

    std::string generate_script(
        const std::string & root,
        const std::string & material,
        const std::string & geometry,
        unsigned int size,
        unsigned int temperature,
        unsigned int index,
        const std::string & uuid_value = ""
    );

    std::vector<std::string> exec(const std::string & cmd);

    ModelEnergies parse(const std::vector<std::string> & lines);

    std::string m_merrill_exe;
    std::shared_ptr<boost::regex> m_regex_energy_joules;
    std::shared_ptr<boost::regex> m_regex_anis_energy;
    std::shared_ptr<boost::regex> m_regex_ext_energy;
    std::shared_ptr<boost::regex> m_regex_demag_energy;
    std::shared_ptr<boost::regex> m_regex_exch1_energy;
    std::shared_ptr<boost::regex> m_regex_exch2_energy;
    std::shared_ptr<boost::regex> m_regex_exch3_energy;
    std::shared_ptr<boost::regex> m_regex_exch4_energy;
    std::shared_ptr<boost::regex> m_regex_tot_energy;
};       

#endif // ENERGIES_H_
