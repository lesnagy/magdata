/**
 * \file   VectorField.h
 * \author L. Nagy
 * 
 * MIT License
 *
 * Copyright (c) [2016] Lesleis Nagy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#ifndef VECTOR_FIELD_H_
#define VECTOR_FIELD_H_

#include <array>
#include <vector>

#include "BasicTypes.h"

class ReadTecplot;
class FieldGradient;

class VectorField
{
public:
  using Vector = std::array<Float, 3>;
  using VectorList = std::vector<Vector>;

  VectorField()
  {}

  VectorField(Size npoints)
  {
    m_vects.resize(npoints);
  }

  inline Size npoints() const { return m_vects.size(); }
  inline const Vector & vector(Index idx) const { return m_vects[idx]; }
  inline const VectorList & vectors() const { return m_vects; }

private:
  VectorList m_vects;

  inline void insert_vector(Float fx, Float fy, Float fz)
  {
    m_vects.push_back({fx, fy, fz});
  }

  inline void set_vector(Index idx, Float fx, Float fy, Float fz)
  {
    m_vects[idx] = {fx, fy, fz};
  }

  inline void clear()
  {
    m_vects.clear();
  }

  Float norm() const;

  friend class ReadTecplot;
  friend class FieldGradient;
};



#endif // VECTOR_FIELD_H_
