/**
 * \file   legacy_directory_utilities.h
 * \author L. Nagy
 * 
 * MIT License
 *
 * Copyright (c) [2016] Lesleis Nagy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <sys/stat.h>

#include <fstream>
#include <iomanip>
#include <sstream>
#include <string>

#include <boost/filesystem.hpp>

#ifndef LEGACY_DIRECTORY_UTILITIES_H_
#define LEGACY_DIRECTORY_UTILITIES_H_

/**
 * Function to check if a file exists.
 */
inline bool file_exists(const std::string & file_name)
{
    struct stat buffer;
    return (stat(file_name.c_str(), &buffer) == 0);
}

/**
 * Types of mgsti files
 */
enum mgsti_tuple_types
{
    TECPLOT,
    STDOUT,
    DAT
};

/**
 * Generate a file name from an mgsti-tuple.
 */
std::string mgsti_file_name(
    unsigned int size,
    unsigned int temperature,
    unsigned int index,
    mgsti_tuple_types type
); 

/**
 * Generate a 'pathed' mgsti file name from an mgsti-tuple.
 */
std::string mgsti_file(
    const std::string & material,
    const std::string & geometry,
    unsigned int size,
    unsigned int temperature,
    unsigned int index,
    mgsti_tuple_types type
);

/**
 * Generate an 'absolute pathed' mgsti file name from an mgsti-tuple with 
 * root directory.
 */
std::string mgsti_file(
    const std::string & root,
    const std::string & material,
    const std::string & geometry,
    unsigned int size,
    unsigned int temperature,
    unsigned int index,
    mgsti_tuple_types type
);

/**
 * Generate mesh file name
 */
std::string mesh_file_name(unsigned int size);

/**
 * Generate a 'pathed' mesh file.
 */
std::string mesh_file(
    const std::string & material, 
    const std::string & geometry,
    unsigned int size
);

/**
 * Generate an 'absolute-pathed' mesh file.
 */
std::string mesh_file(
    const std::string & root,
    const std::string & material, 
    const std::string & geometry,
    unsigned int size
);

#endif // LEGACY_DIRECTORY_UTILITIES_H_
