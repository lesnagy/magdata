/*
 * \file   VectorFieldImage.h
 * \author L. Nagy
 * 
 * MIT License
 *
 * Copyright (c) [2016] Lesleis Nagy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef VECTOR_FIELD_IMAGE_H_
#define VECTOR_FIELD_IMAGE_H_

#include <cmath>

#include <string>

#include "vtkTextProperty.h"
#include "vtkCamera.h"
#include "vtkCubeAxesActor.h"
#include <vtkActor.h>
#include <vtkArrayCalculator.h>
#include <vtkArrowSource.h>
#include <vtkAxesActor.h>
#include <vtkBoundingBox.h>
#include <vtkCellData.h>
#include <vtkColorTransferFunction.h>
#include <vtkContourGrid.h>
#include <vtkCubeSource.h>
#include <vtkDataObjectToTable.h>
#include <vtkDataSetMapper.h>
#include <vtkDoubleArray.h>
#include <vtkElevationFilter.h>
#include <vtkFloatArray.h>
#include <vtkGlyph3D.h>
#include <vtkGradientFilter.h>
#include <vtkGraphicsFactory.h>
#include <vtkLookupTable.h>
#include <vtkMaskPoints.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkPNGWriter.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderLargeImage.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkWindowToImageFilter.h>


#include "BasicTypes.h"
#include "Mesh.h"
#include "ScalarField.h"
#include "VectorField.h"

class VectorFieldImage
{
public:
    void operator () (
        const Mesh & mesh, 
        const VectorField & vf, 
        const std::string & outfile,
        int img_scale,
        double arrow_scale,
        bool with_axes,
        double axes_screen_size,
        double cpx, 
        double cpy, 
        double cpz,
        double cfx,
        double cfy,
        double cfz,
        double theta,
        double phi,
        double r,
        bool with_degrees
    );

private:
    vtkSmartPointer<vtkUnstructuredGrid> 
    create_ugrid(
        const Mesh & mesh
    );

    void 
    set_field(
        vtkSmartPointer<vtkUnstructuredGrid> ugrid, 
        const VectorField  & field
    );

    std::pair<double, double>
    set_helicity(
        vtkSmartPointer<vtkUnstructuredGrid> ugrid
    );

    vtkSmartPointer<vtkActor>
    get_arrows(
        vtkSmartPointer<vtkUnstructuredGrid> ugrid, 
        double hmin, 
        double hmax,
        double arrow_scale
    );

    vtkSmartPointer<vtkLookupTable> 
    build_lut(
        double hmin, 
        double hmax, 
        size_t nlut
    );

};

#endif // VECTOR_FIELD_IMAGE_H_
