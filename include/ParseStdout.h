/**
 * \file   ParseStdout.h
 * \author L. Nagy
 * 
 * MIT License
 *
 * Copyright (c) [2016] Lesleis Nagy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#ifndef PARSE_STDOUT_H_
#define PARSE_STDOUT_H_

#include <cmath>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <tuple>
#include <vector>

#include <boost/regex.hpp>

#include "BasicTypes.h"

class ParseStdout
{
public:
  struct Record {
    int restart_id;
    Float typical_energy;
    bool is_failed;
    Float delta_f;
    Size energy_calls;
    Float final_energy;
    Float delta_f_tol;
    Float sqrt_grad_gtol;
  };

  using SummaryTable = std::vector<Record>;

  ParseStdout() :
    m_float("[+-]?(?=[.]?[0-9])[0-9]*(?:[.][0-9]*)?(?:[Ee][+-]?[0-9]+)?")
  {
    using std::make_shared;

    m_regex_restart = make_shared<boost::regex> (
      "^\\s*RESTART\\s*=\\s*([0-9]+)\\s*$"
    );

    m_regex_typical_energy = make_shared<boost::regex> (
      "^\\s*Typical Energy\\s*:\\s*(" + m_float + ")\\s*$"
    );

    m_regex_converge_status = make_shared<boost::regex> (
      "^.*FAILED TO CONVERGE.*$"
    );

    m_regex_delta_f = make_shared<boost::regex> (
      "^\\s*Delta F negligible\\s*:\\s*(" + m_float + ")\\s*$"
    );

    m_regex_energy_calls = make_shared<boost::regex> (
      "^.*Energy Calls\\s*:\\s*([0-9]+).*$"
    );

    m_regex_final_energy = make_shared<boost::regex> (
      "^.*Final Energy\\s*:\\s*(" + m_float + ").*$"
    );

    m_regex_delta_f_tol = make_shared<boost::regex> (
      "^.*\\s*DeltaF/TOL:\\s*(" + m_float + ").*$"
    );

    m_regex_sqrt_grad_tol = make_shared<boost::regex> (
      "^.*\\s*sqrt\\(grad\\^2/GTOL\\):\\s*(" + m_float + ").*$"
    );

  }

  SummaryTable operator () (const std::string & file_name);

private:
  std::string m_float;

  std::shared_ptr<boost::regex> m_regex_restart;
  std::shared_ptr<boost::regex> m_regex_typical_energy;
  std::shared_ptr<boost::regex> m_regex_converge_status;
  std::shared_ptr<boost::regex> m_regex_delta_f;
  std::shared_ptr<boost::regex> m_regex_energy_calls;
  std::shared_ptr<boost::regex> m_regex_final_energy;
  std::shared_ptr<boost::regex> m_regex_delta_f_tol;
  std::shared_ptr<boost::regex> m_regex_sqrt_grad_tol;
};

#endif // PARSE_STDOUT_H_
