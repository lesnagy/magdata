/**
 * \file   ScalarField.h
 * \author L. Nagy
 * 
 * MIT License
 *
 * Copyright (c) [2016] Lesleis Nagy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#ifndef SCALAR_FIELD_H_
#define SCALAR_FIELD_H_

#include <vector>

#include "BasicTypes.h"

class ReadTecplot;
class FieldGradient;

class ScalarField
{
public:
  using ScalarList = std::vector<Float>;

  ScalarField()
  {}

  ScalarField(Size npoints)
  {
    m_scalars.resize(npoints);
  }

  inline Size npoints() const { return m_scalars.size(); }
  inline Float scalar(Index idx) const { return m_scalars[idx]; }
  inline const ScalarList & scalars() const { return m_scalars; }

private:
  ScalarList m_scalars;

  inline void insert_scalar(Float s)
  {
    m_scalars.push_back(s);
  }

  inline void set_scalar(Index idx, Float s)
  {
    m_scalars[idx] = s;
  }

  inline void clear()
  {
    m_scalars.clear();
  }

  Float sum() const;

  friend class ReadTecplot;
  friend class FieldGradient;
};

#endif // SCALAR_FIELD_H_
