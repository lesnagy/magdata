.. magdata documentation master file, created by
   sphinx-quickstart on Mon Nov  5 10:49:16 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to magdata's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
  

The magdata Python module provides a library of functions and classes useful
for extracting cluster data from micromagetic model information. Currently,
magdata relies on micromagnetic information being stored in a specific
directory structure as highlighed in figure :numref:`legacy_dir_struct`.

.. _legacy_dir_struct:
.. figure:: figures_pdf/legacy_directory_structure.pdf
   :scale: 26%
   :align: center
   :alt: legacy directory structure

   Legacy directory structure. Micromagnetic model files are stored in the
   directory structure outlined. Below some root directory, we have a
   material/geometry pair (mg)-pair. Below this are two directories, the meshes
   which hold finite element mesh information for each model and the lems which
   hold micormagnetic solutions. The mesh files (in the form of patran neural
   files) are numbered according to the mesh size in nanometers equivalent
   spherical volume diamemter (ESVD). The lems files are futher split by size
   and temperature thus forming a material/geometry/size/temperature
   (mgst)-tuple. Each mgst-tuple, then contains N solutions, with each solution
   indexed by some integer forming a material/geometry/size/temperature/index
   (mgsti)-tuple. Each mgsti tuple has 5 files corresponding to it: a) the
   micromagnetic energy log file, b) each component of the magnetization , c)
   the merrill script used to run the model, d) the standard error output file,
   e) the standard output file and f) the tecplot file which contains both
   geometry and moment information.

The clustering module
=====================
.. automodule:: magdata.clustering
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
