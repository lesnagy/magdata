\documentclass[10pt]{article}

%%%%%%%%%%%%
% Packages %
%%%%%%%%%%%%

\usepackage[T1]{fontenc}
\usepackage{bold-extra}
\usepackage{graphicx}
\usepackage{authblk}
\usepackage{gensymb}
\usepackage{chemformula}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{lipsum}
%\usepackage[switch]{lineno}
\usepackage[a4paper, total={6.5in, 9in}]{geometry}
\usepackage{bbm}
\usepackage{bm}
\usepackage{cancel}
\usepackage[symbol]{footmisc}
\usepackage{url}
\usepackage{algorithm2e}
\usepackage{float}
\usepackage[nomarkers,figuresonly]{endfloat}

\newcommand{\eqrefr}[1]{(eqn. \ref{#1})}
\newcommand{\figrefr}[1]{(fig. \ref{#1})}

\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}

\setlength{\parskip}{6pt}
\setlength{\parindent}{0pt}

\newcommand{\vect}[1]{\ensuremath{\bm{#1}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This set of options has all math as typewriter style font.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[subdued, defaultmathsizes, LGRgreek]{mathastext}
\MTnonlettersobeymathxx     % math alphabets will act on (, ), [, ], etc...
\MTexplicitbracesobeymathxx % math alphabets will act on \{ and \}
\MTfamily {\ttdefault}      % we will declare a math version using tt font
\MTgreekfont{lmtt}
\Mathastext [typewriter]    % the math version is called typewriter
\MTversion [normal]{typewriter}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% This set of options has everything as typewriter style font.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\renewcommand{\rmdefault}{\ttdefault}
%\usepackage[defaultmathsizes, LGRgreek]{mathastext}
%\MTnonlettersobeymathxx     % math alphabets will act on (, ), [, ], etc...
%\MTexplicitbracesobeymathxx % math alphabets will act on \{ and \}
%\MTfamily {\ttdefault}      % we will declare a math version using tt font
%\MTgreekfont{lmtt}
%\Mathastext    % the math version is called typewriter

%%%%%%%%%%%
% Authors %
%%%%%%%%%%%

\author[1]{Lesleis Nagy}
\author[2]{Wyn Williams}
\author[1]{Lisa Tauxe}
\affil[1]{Scripps Institute of Oceanography, La Jolla, CA}
\affil[2]{School of GeoSciences, University of Edinburgh, UK}
\date{}

%%%%%%%%%
% Title %
%%%%%%%%%

\title{Some notes on clustering}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Date (put \date{} for no date) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\date{\today}

%%%%%%%%%%%%
% Document %
%%%%%%%%%%%%

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Line numbers (comment out if not needed) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\modulolinenumbers[5]
%\linenumbers

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Title, authors & abstract %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\maketitle

\begin{abstract}

\noindent In order to extract viable end member pairs for path minimization and
subsequent calculation of energy paths, the methods outlined in this document
are used. The source code that accompanies these notes is available on
bitbucket \cite{nagy_et_al-2018a}.
\end{abstract}

%%%%%%%%%%%%
% Document %
%%%%%%%%%%%%
\section*{Description of the software}

There are two parts of the software: 
\begin{itemize} 
    \item a small command line utility called \texttt{magdata} that
        reads a tecplot file, computes some relevant parameters and prints some
        JSON text with the summary data to the standard output.  \item a set of
        python scripts that perform various useful tasks, viz.  
    \begin{itemize}
        \item a utility script called \texttt{magmetadata} that traverses
            the legacy directory structure, computes various statistics on
            micromagnetic strcutrues along with the output of
            \texttt{magdata}.
        \item a utility called \texttt{synchronize} that synchronizes
            \texttt{magdata} scripts across different machines.  \item
            scatter a utility that attempts to find viable end member pairs
            for nudged elastic band NEB minimization.
    \end{itemize}
\end{itemize}

\subsection*{\texttt{magdata} command line utility}
The \texttt{magdata} command line utility takes as input a \texttt{mgsti}-tuple (magnetization, geometry, size, temperature, index) and a file store \texttt{root}. It then finds the files
associated with the \texttt{mgsti}-tuple and computes the following statistics:
\subsubsection*{Integral of the magnetization field}
The `total magnetization' field is the integral of the magnetization over the volume $V$ and is given by
\begin{flalign}
    \mathbf{M}_\mathrm{tot} = \int_V \left( \mathbf{M}(\mathbf{x})\right) \;d\mathbf{x}. \label{eqn:M_tot}
\end{flalign}
\subsubsection*{Integral of the vorticity field}
The vorticity field is given by 
\begin{flalign}
    \mathbf{V}(\mathbf{x}) = \nabla \times \mathbf{M}(\mathbf{x}), \label{eqn:V}
\end{flalign}
and so the integral of the vorticity field ($V_\mathrm{tot}$) is
\begin{flalign}
    \mathbf{V}_\mathrm{tot} = \int_V \left( \mathbf{V}(\mathbf{x})\right) \;d\mathbf{x}. \label{eqn:V_tot}
\end{flalign}
\subsubsection*{Integral of the helicity field}
The helicity field is given by
\begin{flalign}
    H = \mathbf{M}(\mathbf{x}) \cdot \mathbf{V}(\mathbf{x}) = \mathbf{M}(\mathbf{x}) \cdot \left(\nabla \times \mathbf{M}(\mathbf{x})\right), \label{eqn:H}
\end{flalign}
and so the total helicity is
\begin{flalign}
    H_\mathrm{tot} = \int_V H(\mathbf{x}) \;d\mathbf{x}. \label{eqn:H_tot}
\end{flalign}
Once these quantities have been calculated, they are compiled in to a JSON file
with the following format

\section*{\texttt{magmetadata} script}
This script descends through the directory structure shown in
\figrefr{fig:legacy_directory_structure} and executes the \texttt{magdata} command
line utility. It then makes use of \texttt{merrill} in order to calculate the
true energies of each structure. 
\begin{figure}
    \centering
    \includegraphics[width=0.8\linewidth]{figures/legacy_directory_structure.pdf}
    \caption[Legacy directory strucure]{The legacy directory structure. All
    micromagnetic models are stored according to the structure outlined here
    with respect to some \texttt{root} directory. The \texttt{mgst} (blue) and
    \texttt{mgsti} tuples (orange) are shown with respect to the directory
    structure. The highlighted boxes show a complete \texttt{mgsti} path that
    relates to a model, note how a model actually consists of five files: (a) log
    file, (b) dat file of magneitzation vectors, (c) the
    \texttt{merrill} script, (d) the standard error output and (e) the standard output file.}
    \label{fig:legacy_directory_structure}
\end{figure}
These two outputs are combined into a final output called
\texttt{mm\_stat\_record} \figrefr{fig:mm_stat_record}. For each index, $i$, in a \texttt{mgst}-tuple
(material, geometry, size, temperature) an \texttt{mm\_stat\_record} is
calculated and all \texttt{mm\_stat\_record}s are stored in a file called
\texttt{summary\_stats.json} which is essentialy a table of
\texttt{mm\_stat\_record}s in JSON form.
\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{figures/mm_stat_record.pdf}
    \caption[Schematic representation of \texttt{mm\_stat\_record}]{Schematic 
    representation of \texttt{mm\_stat\_record} shows all the fields combined
    from the output of \texttt{magdata}, \texttt{magmetadata} and
    \texttt{merrill}. For each index in an \texttt{mgsti}-tuple a data
    structure like this is calculated. It is then written (along with
    \texttt{mgsti}-tuples for all the other indices) to a file called
    \texttt{summary\_stats.json}}.
    \label{fig:mm_stat_record}
\end{figure}

\subsection*{The directory structure}
The directory structure traversed by \texttt{magmetadata} is illustrated in
\figrefr{fig:legacy_directory_structure} and it is expected that the layout is
always like this.

\section*{\texttt{clustering} library}

This library is a collection of tools for clustering together solutions by
structure. The method of clustering is hierarchical as outlined in
\figrefr{fig:cluster_hierarchy}. 
\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{figures/cluster_hierarchy.pdf}
    \caption[Schematic representation of cluster hierarchy]{Schematic 
    representation of cluster hierarchy.}
    \label{fig:cluster_hierarchy}
\end{figure}
Firstly all \texttt{mm\_stat\_record}s within a list are clustered by
$H_\mathrm{tot}$ \eqrefr{eqn:H_tot}, this should result in no more than three
clusters:
\begin{itemize}
    \item left handed,
    \item neutral,
    \item right handed.
\end{itemize}
The next level of clustering results in extraction of distinct clusters, this
is achieved using the total $E_\mathrm{tot}$ and anisotropy $E_\mathrm{anis}$
energies which have been calculated from \texttt{merrill}. The final clustering
is performed on direction $M_\mathrm{tot}$ as defined in \eqrefr{eqn:M_tot}
within each cluster.

Intra-cluster pairs are those pairs where the angle between any two direction
clusters are minimized (the clustering should ensure that we do not pick
end-members that are too close to each other). Intra-cluster pairs are selected
according to similar criteria \emph{i.e.} the end-member pairs minimize the
angle between $M_\mathrm{tot}$, however this time each end member must be from
a different structure type.


\subsection*{Clustering 1}
Cluster 1 is a clustering based on the helicity value only

\subsection*{Clustering 2}
Cluster 2 is a clustering based on the dbscan method using the total energy
along with the anisotropy energy. It clusters based on the DBSCAN method with
the following default parameters: 
\begin{flalign*}
    &eps = 0.05 \\
    &min\_samples = 3
\end{flalign*}
The value $0.05$ for eps was found to be empirically `good' for several
clusters, based primarily on the cuboctahedron of $5\%$ elongation. The value
for $min\_samples$ is chosen to be $3$ since a rule of thumb is that
$min\_samples \geq D + 1$, where $D$ is the dimensionality of the cluster space
(in this case $2$).

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{figures/mm_clustered_stat_records.pdf}
    \caption[Schematic representation of lustered \texttt{mm\_stat\_record}s]{
     Schematic representation of lustered \texttt{mm\_stat\_record}s. This data
     structure is represented as a hash with each \texttt{mm\_stat\_record} indexed
     by the cluster to which it belongs}
     \label{fig:clustered_stat_records}
\end{figure}


%%%%%%%%%%%%%%%%
% Bibliography %
%%%%%%%%%%%%%%%%
\bibliographystyle{acm}
\bibliography{bibliography}

%%%%%%%%%%%
% Figures %
%%%%%%%%%%%

\end{document}
