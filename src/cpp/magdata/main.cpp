/**
 * \file   main.cpp
 * \author L. Nagy
 * 
 * MIT License
 *
 * Copyright (c) [2016] Lesleis Nagy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <cmath>
#include <exception>
#include <iomanip>
#include <iostream>
#include <string>
#include <sstream>
#include <utility>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/range/adaptor/reversed.hpp>

#include "Energies.h"
#include "FieldGradient.h"
#include "Integrate.h"
#include "Mesh.h"
#include "ParseStdout.h"
#include "ReadTecplot.h"
#include "VectorField.h"

#include "legacy_directory_utilities.h"

int main(int argc, char *argv[])
{
  using std::cout;
  using std::endl;
  using std::exception;
  using std::isnan;
  using std::right;
  using std::scientific;
  using std::setfill;
  using std::setprecision;
  using std::setw;
  using std::string;
  using std::stringstream;
  using std::tie;

  using boost::adaptors::reverse;
  using boost::program_options::options_description;
  using boost::program_options::value;
  using boost::program_options::variables_map;

  Mesh mesh;
  VectorField M;
  ReadTecplot read_tecplot;
  Integrate integrate;
  FieldGradient field_gradient;

  VectorField V;
  ScalarField H;

  ParseStdout parse_stdout;

  try {
    const string default_root = "/geos/d22/shared/MMDatabase/legacy";

    string root = "";
    string material = "";
    string geometry = "";
    int size = 0;
    int temperature = 0;
    int index = 0;
    string merrill_exe = "";
    string uidseed = "";

    options_description desc("Allowed options");
    desc.add_options()
    ("help,h", "print usage message")
    ("root",  
      value<string>()->default_value(default_root), 
      "the directory database root"
    )
    ("material", value<string>()->required(), "the material name")
    ("geometry", value<string>()->required(), "geometry")
    ("size", value<int>()->required(), "the geometry size")
    ("temperature", value<int>()->required(), "the material temperature")
    ("index", value<int>()->required(), "the model index")
    ("merrill", value<string>()->default_value("merrill"), "The merrill executable")
    ("uidseed", value<string>()->default_value(""), "A unique seed value identifier")
    ;

    variables_map vm;
    store(parse_command_line(argc, argv, desc), vm);

    if (vm.count("help")) {
      cout << desc << endl;
      return 0;
    }

    if (vm.count("root")) {
      root = vm["root"].as<string>();
    } else {
      root = default_root;
    }

    if (vm.count("material")) {
      material = vm["material"].as<string>();
    } else {
      cout << "Parameter 'material' is required." << endl;
      cout << desc << endl;
      return 0;
    }

    if (vm.count("geometry")) {
      geometry = vm["geometry"].as<string>();
    } else {
      cout << "Parameter 'geometry' is required." << endl;
      cout << desc << endl;
      return 0;
    }

    if (vm.count("size")) {
      size = vm["size"].as<int>();
    } else {
      cout << "Parameter 'size' is required." << endl;
      cout << desc << endl;
      return 0;
    }

    if (vm.count("temperature")) {
      temperature = vm["temperature"].as<int>();
    } else {
      cout << "Parameter 'temperature' is required." << endl;
      cout << desc << endl;
      return 0;
    }

    if (vm.count("index")) {
      index = vm["index"].as<int>();
    } else {
      cout << "Parameter 'index' is required." << endl;
      cout << desc << endl;
      return 0;
    }

    if (vm.count("merrill")) {
      merrill_exe = vm["merrill"].as<string>();
    } else {
      merrill_exe = "merrill";
    }

    if (vm.count("uidseed")) {
      uidseed = vm["uidseed"].as<string>();
    } else {
      uidseed = "uidseed";
    }

    string model_file = mgsti_file(
        root, material, geometry, size, temperature, index, TECPLOT
    );

    string stdout_file = mgsti_file(
        root, material, geometry, size, temperature, index, STDOUT
    );

    // Read the tecplot file
    tie(mesh, M) = read_tecplot(model_file);
    tie(V, H) = field_gradient(mesh, M);

    VectorField::Vector Mtot = integrate(mesh, M);
    VectorField::Vector Vtot = integrate(mesh, V);
    double Htot = integrate(mesh, H);

    ParseStdout::SummaryTable energy_summary = parse_stdout(stdout_file);

    Energies energies(merrill_exe);
    ModelEnergies model_energies = energies(
        root, material, geometry, size, temperature, index, uidseed
    );

    double typical_energy = 0.0;
    double final_energy = 0.0;
    for (auto r : reverse(energy_summary)) {
      if (!isnan(r.typical_energy) && !isnan(r.final_energy) && !r.is_failed) {
        //failed = false;
        typical_energy = r.typical_energy;
        final_energy = r.final_energy;

        stringstream json;
        json << "{" << endl
             << "\t\"material\" : \""  << material                             << "\"," << endl
             << "\t\"geometry\" : \""  << geometry                             << "\"," << endl
             << "\t\"size\" : "        << size                                 << ","   << endl
             << "\t\"temperature\" : " << temperature                          << ","   << endl
             << "\t\"index\" : \""     << index                                << "\"," << endl
             << "\t\"status\" : \""    << "SUCCESS"                            << "\"," << endl
             << "\t\"volume\" : "      << setprecision(20) << mesh.volume()    << ","   << endl
             << "\t\"esvd\" : "        << setprecision(20) << mesh.esvd()*1000 << ","   << endl
             << "\t\"M_tot_x\" : "     << setprecision(20) << Mtot[0]          << ","   << endl
             << "\t\"M_tot_y\" : "     << setprecision(20) << Mtot[1]          << ","   << endl
             << "\t\"M_tot_z\" : "     << setprecision(20) << Mtot[2]          << ","   << endl
             << "\t\"V_tot_x\" : "     << setprecision(20) << Vtot[0]          << ","   << endl
             << "\t\"V_tot_y\" : "     << setprecision(20) << Vtot[1]          << ","   << endl
             << "\t\"V_tot_z\" : "     << setprecision(20) << Vtot[2]          << ","   << endl
             << "\t\"H_tot\" : "       << setprecision(20) << Htot             << ","   << endl
             << "\t\"E_typical\" : "   << setprecision(20) << typical_energy   << ","   << endl
             << "\t\"E_final\" : "     << setprecision(20) << final_energy     << ","   << endl
             << "\t\"anis\" : "  << setprecision(20) << model_energies.anis    << ","   << endl
             << "\t\"ext\" : "   << setprecision(20) << model_energies.ext     << ","   << endl
             << "\t\"demag\" : " << setprecision(20) << model_energies.demag   << ","   << endl
             << "\t\"exch1\" : " << setprecision(20) << model_energies.exch1   << ","   << endl
             << "\t\"exch2\" : " << setprecision(20) << model_energies.exch2   << ","   << endl
             << "\t\"exch3\" : " << setprecision(20) << model_energies.exch3   << ","   << endl
             << "\t\"exch4\" : " << setprecision(20) << model_energies.exch4   << ","   << endl
             << "\t\"tot\" : "   << setprecision(20) << model_energies.tot     << ""    << endl
             << "}"; 
        cout << json.rdbuf() << endl;

        break;
      } else {
        stringstream json;
        json << "{" << endl
             << "\t\"material\" : \""    << material    << "\"," << endl
             << "\t\"geometry\" : \""    << geometry    << "\"," << endl
             << "\t\"size\" : \""        << size        << "\"," << endl
             << "\t\"temperature\" : \"" << temperature << "\"," << endl
             << "\t\"index\" : \""       << index       << "\"," << endl
             << "\t\"status\" : \""      << "FAIL"      << "\"" << endl
             << "}"; 
        cout << json.rdbuf() << endl;

        break;
      }
    }
  } catch (exception & e) {
    cout << e.what() << endl;
    cout << "Failed, please check that file exists or use -h for correct options." << endl;
  }

  return 0;

}






