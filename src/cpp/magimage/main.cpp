/*
 * \file   main.cpp
 * \author L. Nagy
 * 
 * MIT License
 *
 * Copyright (c) [2016] Lesleis Nagy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <cmath>
#include <exception>
#include <iomanip>
#include <iostream>
#include <string>
#include <sstream>
#include <utility>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/range/adaptor/reversed.hpp>

#include "Mesh.h"
#include "VectorField.h"
#include "ReadTecplot.h"
#include "VectorFieldImage.h"

int main(int argc, char **argv)
{
    using std::cout;
    using std::endl;
    using std::exception;
    using std::isnan;
    using std::right;
    using std::scientific;
    using std::setfill;
    using std::setprecision;
    using std::setw;
    using std::string;
    using std::stringstream;
    using std::tie;

    using boost::adaptors::reverse;
    using boost::program_options::options_description;
    using boost::program_options::value;
    using boost::program_options::variables_map;
    using boost::program_options::bool_switch;

    try {
        Mesh mesh;
        VectorField f;
        ReadTecplot read_tecplot;
        VectorFieldImage vector_field_image;

        string tecplot_file;

        string png_file;
        int imgscale;

        double arrow_scale;

        bool axes = false;
        double axesss = NAN;

        double cpx = NAN;
        double cpy = NAN;
        double cpz = NAN;

        double cfx = NAN;
        double cfy = NAN;
        double cfz = NAN;

        double cptheta = NAN;
        double cpphi = NAN;
        double cpr = NAN;
        bool degrees = true;

        options_description desc("Allowed options");
        desc.add_options()
        ("help,h",  "print usage message")
        ("tecplot", value<string>(&tecplot_file), "the tecplot file to visualize")
        ("png", value<string>(&png_file), "the output png file name")
        ("imgscale", value<int>(&imgscale)->default_value(20), "image scale (magnification) factor")
        ("arrowscale", value<double>(&arrow_scale)->default_value(0.01), "the arrow size scale")
        ("axes", bool_switch(&axes)->default_value(false), "should axes be added to the output image")
        ("axesss", value<double>(&axesss)->default_value(40.0), "axes screen size")
        ("cpx", value<double>(&cpx), "camera position x")
        ("cpy", value<double>(&cpy), "camera position y")
        ("cpz", value<double>(&cpz), "camera position z")
        ("cfx", value<double>(&cfx), "camera focus x")
        ("cfy", value<double>(&cfy), "camera focus y")
        ("cfz", value<double>(&cfz), "camera focus z")
        ("cptheta", value<double>(&cptheta), "camera orientation theta [0, 360)")
        ("cpphi", value<double>(&cpphi), "camera orientation in phi [0, 180)")
        ("cpr", value<double>(&cpr), "camera distance")
        ("degrees", bool_switch(&degrees)->default_value(true), "camera orientation in degrees")
        ;

        variables_map vm;
        store(parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
            cout << desc << endl;
            return 0;
        }

        if (vm.count("tecplot")) {
          tecplot_file = vm["tecplot"].as<string>();
        } else {
          cout << "Parameter 'tecplot' is required." << endl;
          cout << desc << endl;
          return 0;
        }

        if (vm.count("png")) {
          png_file = vm["png"].as<string>();
        } else {
          cout << "Parameter 'png' is required." << endl;
          cout << desc << endl;
          return 0;
        }


        if (vm.count("imgscale")) {
          imgscale = vm["imgscale"].as<int>();
        } else {
          imgscale = 20;
        }

        if (vm.count("arrowscale")) {
          arrow_scale = vm["arrowscale"].as<double>();
        } else {
          arrow_scale = 0.01;
        }

        if (vm.count("axes")) {
        axes = vm["axes"].as<bool>();
        } else {
          axes = false;
        }

        if (vm.count("axesss")) {
          axesss = vm["axesss"].as<double>();
        } else {
          axesss = 40.0;
        }

        if (vm.count("cpx")) {
        cpx = vm["cpx"].as<double>();
        } else {
          cpx = NAN;
        }

        if (vm.count("cpy")) {
          cpy = vm["cpy"].as<double>();
        } else {
          cpy = NAN;
        }

        if (vm.count("cpz")) {
          cpz = vm["cpz"].as<double>();
        } else {
          cpx = NAN;
        }

        if (vm.count("cfx")) {
          cfx = vm["cfx"].as<double>();
        } else {
          cfx = NAN;
        }

        if (vm.count("cfy")) {
          cfy = vm["cfy"].as<double>();
        } else {
          cfy = NAN;
        }

        if (vm.count("cfz")) {
          cfz = vm["cfz"].as<double>();
        } else {
          cfz = NAN;
        }

        if (vm.count("cptheta")) {
          cptheta = vm["cptheta"].as<double>();
        } else {
          cptheta = NAN;
        }

        if (vm.count("cpphi")) {
          cpphi = vm["cpphi"].as<double>();
        } else {
          cpphi = NAN;
        }

        if (vm.count("cpr")) {
          cpr = vm["cpr"].as<double>();
        } else {
          cpr = NAN;
        }

        if (vm.count("degrees")) {
          degrees = vm["degrees"].as<bool>();
        } else {
          degrees = NAN;
        }

        tie(mesh, f) = read_tecplot(tecplot_file);

        vector_field_image(
            mesh, f, png_file, imgscale, arrow_scale, axes, axesss, 
            cpx, cpy, cpz, cfx, cfy, cfz,
            cptheta, cpphi, cpr, degrees
        );

    } catch (exception & e) {
        cout << e.what() << endl;
        cout << "Failed, please use -h for correct options." << endl;
    }

    return 0;
}

