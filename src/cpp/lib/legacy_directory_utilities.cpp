/**
 * \file   legacy_directory_utilities.cpp
 * \author L. Nagy
 * 
 * MIT License
 *
 * Copyright (c) [2016] Lesleis Nagy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include "legacy_directory_utilities.h"

/**
 * Generate a file name from an mgsti-tuple.
 */
std::string mgsti_file_name(
    unsigned int size,
    unsigned int temperature,
    unsigned int index,
    mgsti_tuple_types type
) {
    using std::stringstream;
    using std::setfill;
    using std::setw;

    stringstream ss;
    switch (type) {
        case TECPLOT: {
            ss << size << "nm_" << temperature << "C_mag_" 
               << setfill('0') << setw(4) << index
               << "_mult.tec";
        } break;
        case STDOUT: {
            ss << "stdout_lcl_" << size << "nm_" << temperature << "C_"
               << setfill('0') << setw(4) << index;
        } break;
        case DAT: {
            ss << size << "nm_" << temperature << "C_mag_"
               << setfill('0') << setw(4) << index
               << ".dat";
        } break;
    }
    return ss.str();
}

/**
 * Generate a 'pathed' mgsti file name from an mgsti-tuple.
 */
std::string mgsti_file(
    const std::string & material,
    const std::string & geometry,
    unsigned int size,
    unsigned int temperature,
    unsigned int index,
    mgsti_tuple_types type
) {
    using std::stringstream;
    using std::setfill;
    using std::setw;

    const char sep = boost::filesystem::path::preferred_separator;

    stringstream ss;
    ss << material << sep 
       << geometry << sep 
       << "lems" << sep
       << size << "nm" << sep
       << temperature << "C" << sep
       << mgsti_file_name(size, temperature, index, type);

    return ss.str();
}

/**
 * Generate an 'absolute pathed' mgsti file name from an mgsti-tuple with 
 * root directory.
 */
std::string mgsti_file(
    const std::string & root,
    const std::string & material,
    const std::string & geometry,
    unsigned int size,
    unsigned int temperature,
    unsigned int index,
    mgsti_tuple_types type
) {
    using std::stringstream;
    using std::setfill;
    using std::setw;

    const char sep = boost::filesystem::path::preferred_separator;

    stringstream ss;
    ss << root << sep 
       << mgsti_file(material, geometry, size, temperature, index, type);

    return ss.str();
}

/**
 * Generate mesh file name
 */
std::string mesh_file_name(unsigned int size)
{
    using std::stringstream;
    using std::setfill;
    using std::setw;

    stringstream ss;
    ss << size << ".pat";

    return ss.str();
}

/**
 * Generate a 'pathed' mesh file.
 */
std::string mesh_file(
    const std::string & material, 
    const std::string & geometry,
    unsigned int size
) {
    using std::stringstream;
    using std::setfill;
    using std::setw;

    const char sep = boost::filesystem::path::preferred_separator;

    stringstream ss;

    ss << material << sep 
       << geometry << sep
       << "meshes" << sep
       << mesh_file_name(size);

    return ss.str();
}

/**
 * Generate an 'absolute-pathed' mesh file.
 */
std::string mesh_file(
    const std::string & root,
    const std::string & material, 
    const std::string & geometry,
    unsigned int size
) {
    using std::stringstream;
    using std::setfill;
    using std::setw;

    const char sep = boost::filesystem::path::preferred_separator;

    stringstream ss;

    ss << root << sep << mesh_file(material, geometry, size);

    return ss.str();
}
