/**
 * \file   Mesh.cpp
 * \author L. Nagy
 * 
 * MIT License
 *
 * Copyright (c) [2016] Lesleis Nagy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include "Mesh.h"

void Mesh::v2v_conn()
{
    m_v_neighbours.clear();
    m_v_neighbours.resize(m_verts.size());

    for (auto e : m_elems) {
        m_v_neighbours[e[0]].insert(e[1]);
        m_v_neighbours[e[0]].insert(e[2]);
        m_v_neighbours[e[0]].insert(e[3]);

        m_v_neighbours[e[1]].insert(e[2]);
        m_v_neighbours[e[1]].insert(e[3]);

        m_v_neighbours[e[2]].insert(e[3]);
    }
}

void Mesh::e2e_conn_f()
{
    m_fe_neighbours.clear();
    m_fe_neighbours.resize(m_elems.size());

    FaceElementList f_elems;
    for (Index i = 0; i < m_elems.size(); ++i) {
        Element se = sort(m_elems[i]);
        f_elems[{se[0], se[1], se[2]}].insert(i);
        f_elems[{se[0], se[1], se[3]}].insert(i);
        f_elems[{se[0], se[2], se[3]}].insert(i);
        f_elems[{se[1], se[2], se[3]}].insert(i);
    }

    for (auto p : f_elems) {
        // TODO
    }

}

Float Mesh::volume() const
{
  Float vol = 0.0;

  for (auto elem : m_elems) {
    Float x1  = m_verts[elem[0]][0];
    Float y1  = m_verts[elem[0]][1];
    Float z1  = m_verts[elem[0]][2];

    Float x2  = m_verts[elem[1]][0];
    Float y2  = m_verts[elem[1]][1];
    Float z2  = m_verts[elem[1]][2];

    Float x3  = m_verts[elem[2]][0];
    Float y3  = m_verts[elem[2]][1];
    Float z3  = m_verts[elem[2]][2];

    Float x4  = m_verts[elem[3]][0];
    Float y4  = m_verts[elem[3]][1];
    Float z4  = m_verts[elem[3]][2];

    vol = vol + tetra_volume (x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4);
  }

  return vol;
}

Float Mesh::esvd() const 
{
  Float vol = volume();

  return pow(6.0*vol/M_PI, 1.0/3.0);
}

Float Mesh::tetra_volume (
  Float x1, Float y1, Float z1,
  Float x2, Float y2, Float z2,
  Float x3, Float y3, Float z3,
  Float x4, Float y4, Float z4
) const {
  return fabs(-(x2*y3*z1) + x2*y4*z1 + x1*y3*z2 - x1*y4*z2 + x2*y1*z3 
    - x1*y2*z3 + x1*y4*z3 - 
     x2*y4*z3 + x4*(-(y2*z1) + y3*z1 + y1*z2 - y3*z2 - y1*z3 + y2*z3) + 
     (-(x2*y1) + x1*y2 - x1*y3 + x2*y3)*z4 + 
     x3*(-(y4*z1) - y1*z2 + y4*z2 + y2*(z1 - z4) + y1*z4))/6;
}

Mesh::Vertex Mesh::tetra_middle(
  Float x1, Float y1, Float z1,
  Float x2, Float y2, Float z2,
  Float x3, Float y3, Float z3,
  Float x4, Float y4, Float z4
) const {
    Float mid_x = (x1 + x2 + x3 + x4)/4.0;
    Float mid_y = (y1 + y2 + y3 + y4)/4.0;
    Float mid_z = (z1 + z2 + z3 + z4)/4.0;

    return {mid_x, mid_y, mid_z};
}
