/*
 * \file   FieldGradient.cpp
 * \author L. Nagy
 * 
 * MIT License
 *
 * Copyright (c) [2016] Lesleis Nagy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "VectorFieldImage.h"

void VectorFieldImage::operator () (
    const Mesh & mesh, 
    const VectorField & vf, 
    const std::string & outfile,
    int img_scale,
    double arrow_scale,
    bool with_axes,
    double axes_screen_size,
    double cpx, 
    double cpy, 
    double cpz,
    double cfx,
    double cfy,
    double cfz,
    double theta,
    double phi,
    double r,
    bool with_degrees

) {
    using std::isnan;
    using std::tie;

    auto ugrid = create_ugrid(mesh);
    set_field(ugrid, vf);

    double hmin, hmax;
    tie(hmin, hmax) = set_helicity(ugrid);

    auto arrows = get_arrows(ugrid, hmin, hmax, arrow_scale);
    
    auto renderer = vtkSmartPointer<vtkRenderer>::New();
    auto window = vtkSmartPointer<vtkRenderWindow>::New();
    window->SetOffScreenRendering(1);
    window->AddRenderer(renderer);

    renderer->AddActor(arrows);
    renderer->SetBackground(1, 1, 1);

    double bbox[6];
    ugrid->GetBounds(bbox);
    double bbox_max = -1E100;

    std::cout << "Bounding box: " << std::endl;
    for (size_t i = 0; i < 6; ++i) {
        std::cout << std::scientific 
                  << std::setw(20) 
                  << std::setprecision(10) 
                  << bbox[i] << endl;
        if (bbox[i] > bbox_max) {
            bbox_max = bbox[i];
        }
    }

    if (with_axes) {
        auto axes = vtkSmartPointer<vtkCubeAxesActor>::New();
        axes->SetBounds(bbox);
        axes->SetCamera(renderer->GetActiveCamera());
        axes->SetCornerOffset(0.0);
        axes->SetScreenSize(axes_screen_size);

        axes->GetXAxesLinesProperty()->SetColor( 1., 0., 0. );
        axes->GetTitleTextProperty(0)->SetColor( 1., 0., 0. );
        axes->GetLabelTextProperty(0)->SetColor( .8, 0., 0. );

        axes->GetYAxesLinesProperty()->SetColor( 1., 0., 0. );
        axes->GetTitleTextProperty(1)->SetColor( 1., 0., 0. );
        axes->GetLabelTextProperty(1)->SetColor( .8, 0., 0. );

        axes->GetZAxesLinesProperty()->SetColor( 1., 0., 0. );
        axes->GetTitleTextProperty(2)->SetColor( 1., 0., 0. );
        axes->GetLabelTextProperty(2)->SetColor( .8, 0., 0. );

        renderer->AddViewProp(axes);
    }

    renderer->ResetCamera();
    vtkCamera *camera = renderer->GetActiveCamera();

    // If Camera position is given in theta, phi 
    if ( !isnan(theta) && !isnan(phi) ) {
        // Get current camera position distance
        double *pos = camera->GetPosition();
        double rr = 0.0;
        if (!isnan(r)) {
            rr = r;
        } else {
            rr = sqrt(pos[0]*pos[0] + pos[1]*pos[1] + pos[2]*pos[2]);
        }

        double D_TO_G = 0.0;
        if (with_degrees) {
            cout << "WITH DEGREES" << endl;
            D_TO_G = M_PI/180.0;
        } else {
            D_TO_G = 1.0;
        }

        
        cout << "theta: " << theta << endl;
        cout << "phi:   " << phi << endl;
        cout << "D_TO_G: " << D_TO_G << endl;

        double cx = rr*cos(theta*D_TO_G)*sin(phi*D_TO_G);
        double cy = rr*sin(theta*D_TO_G)*sin(phi*D_TO_G);
        double cz = rr*cos(phi*D_TO_G);
        
        camera->SetPosition(cx, cy, cz);
    }

    // If camera position is given (note will override theta/phi 
    if ( !isnan(cpx) && !isnan(cpy) && !isnan(cpz) ) {
        camera->SetPosition(cpx, cpy, cpz);
    }

    // If camera focal point is given
    if ( !isnan(cfx) && !isnan(cfy) && !isnan(cfz) ) {
        camera->SetFocalPoint(cfx, cfy, cfz);
    }


    // Output camera information to stdout

    std::cout << "Camera position:" << std::endl;
    double *pos = camera->GetPosition();
    for (size_t i = 0; i < 3; ++i) {
        std::cout << std::scientific 
                  << std::setw(20) 
                  << std::setprecision(10) 
                  << pos[i] << std::endl;
    }

    std::cout << "Camera focus:" << std::endl;
    double *foc = camera->GetFocalPoint();
    for (size_t i = 0; i < 3; ++i) {
        std::cout << std::scientific 
                  << std::setw(20) 
                  << std::setprecision(10) 
                  << foc[i] << std::endl;
    }

    //camera->SetPosition(pos[0]+0.01, pos[1]+0.01, pos[2]+0.01);
    //camera->SetFocalPoint(0.0, 0.0, 0.0);
    window->Render();

    auto window_to_image = vtkSmartPointer<vtkWindowToImageFilter>::New();
    window_to_image->SetInput(window);
    window_to_image->SetScale(img_scale);
    window_to_image->Update();

    auto writer = vtkSmartPointer<vtkPNGWriter>::New();
    writer->SetFileName(outfile.c_str());
    writer->SetInputConnection(window_to_image->GetOutputPort());
    writer->Write();
}


vtkSmartPointer<vtkUnstructuredGrid> 
VectorFieldImage::create_ugrid(const Mesh & mesh)
{
    auto ugrid = vtkSmartPointer<vtkUnstructuredGrid>::New();

    auto vert = mesh.vertices();
    auto conn = mesh.elements();

    // Add vertices to the unstructured grid.
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    points->SetNumberOfPoints(vert.size());
    for (size_t i = 0; i < vert.size(); ++i) {
        points->SetPoint(i, vert[i][0], vert[i][1], vert[i][2]);
    }
    ugrid->SetPoints(points);

    // Add connectivity information to unstructured grid.  
    for (size_t i = 0; i < conn.size(); ++i) {
        vtkIdType element[4] = {
            (long long)conn[i][0], 
            (long long)conn[i][1], 
            (long long)conn[i][2], 
            (long long)conn[i][3]
        }; 
        ugrid->InsertNextCell(VTK_TETRA, 4, element);
    }

    return ugrid;
}

void 
VectorFieldImage::set_field(
    vtkSmartPointer<vtkUnstructuredGrid> ugrid, 
    const VectorField & vf
) {
    auto field = vf.vectors();
    vtkSmartPointer<vtkDoubleArray> f = 
        vtkSmartPointer<vtkDoubleArray>::New();
    f->SetName("M");
    f->SetNumberOfComponents(3);
    f->SetNumberOfTuples(field.size());

    for (size_t i = 0; i < field.size(); ++i) {
        double fd[3] = {
            (double)field[i][0], 
            (double)field[i][1] , 
            (double)field[i][2]
        };
        f->SetTuple(i, fd);
    }
    ugrid->GetPointData()->AddArray(f);
}

std::pair<double, double> 
VectorFieldImage::set_helicity(vtkSmartPointer<vtkUnstructuredGrid> ugrid) 
{
    using std::make_pair;

    vtkSmartPointer<vtkGradientFilter> vorticity =
        vtkSmartPointer<vtkGradientFilter>::New();

    vtkSmartPointer<vtkArrayCalculator> helicity =
        vtkSmartPointer<vtkArrayCalculator>::New();

    // Vorticity.
    vorticity->ComputeVorticityOn();
    vorticity->SetInputArrayToProcess(0, 0, 0, 0, "M");
    vorticity->SetVorticityArrayName("V");
    vorticity->SetInputData(ugrid);
    vorticity->Update();

    // Helicity.
    // helicity->SetAttributeModeToUsePointData(); // deprecated in > v8.0
    helicity->SetAttributeType(vtkDataObject::POINT);
    helicity->AddVectorArrayName("M");
    helicity->AddVectorArrayName("V");
    helicity->SetResultArrayName("H");
    helicity->SetFunction("M.V");
    helicity->SetInputData(vorticity->GetOutput());
    helicity->Update();

    // Add helicity field.
    vtkUnstructuredGrid * hug = 
        vtkUnstructuredGrid::SafeDownCast(helicity->GetOutput());

    vtkSmartPointer<vtkDoubleArray> h =
        vtkDoubleArray::SafeDownCast(hug->GetPointData()->GetArray("H"));

    vtkSmartPointer<vtkDoubleArray> hd = vtkSmartPointer<vtkDoubleArray>::New();
    hd->SetName("H");
    hd->SetNumberOfComponents(1);
    hd->SetNumberOfTuples(h->GetNumberOfTuples());

    double hmin = 1E12, hmax = -1E12;
    for (int i = 0; i < h->GetNumberOfTuples(); ++i) {
        hmin = (h->GetValue(i) < hmin) ? h->GetValue(i) : hmin;
        hmax = (h->GetValue(i) > hmax) ? h->GetValue(i) : hmax;
        hd->SetValue(i, h->GetTuple(i)[0]);
    }
    ugrid->GetPointData()->AddArray(hd);

    ugrid->GetPointData()->SetActiveVectors("M");
    ugrid->GetPointData()->SetActiveScalars("H");

    return make_pair(hmin, hmax);
}

vtkSmartPointer<vtkActor> VectorFieldImage::get_arrows(
    vtkSmartPointer<vtkUnstructuredGrid> ugrid, 
    double hmin, double hmax,
    double arrow_scale
) {
    auto arrow_source = vtkSmartPointer<vtkArrowSource>::New();
    arrow_source->SetShaftResolution(10);
    arrow_source->SetTipResolution(30);

    auto arrow_transform = vtkSmartPointer<vtkTransform>::New();
    arrow_transform->Translate(-0.5, 0.0, 0.0);

    auto arrow_transform_filter = 
        vtkSmartPointer<vtkTransformPolyDataFilter>::New();
    arrow_transform_filter->SetTransform(arrow_transform);
    arrow_transform_filter->SetInputConnection(arrow_source->GetOutputPort());

    auto arrow_glyph = vtkSmartPointer<vtkGlyph3D>::New();
    arrow_glyph->SetInputData(ugrid);
    arrow_glyph->SetSourceConnection(arrow_transform_filter->GetOutputPort());
    arrow_glyph->SetScaleModeToScaleByVector();
    arrow_glyph->SetVectorModeToUseVector();
    arrow_glyph->SetColorModeToColorByScalar();
    arrow_glyph->ScalingOn();
    arrow_glyph->OrientOn();
    arrow_glyph->SetScaleFactor(arrow_scale);
    arrow_glyph->Update();

    auto lut = build_lut(hmin, hmax, 1000);
    auto arrow_glyph_poly_data_mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    arrow_glyph_poly_data_mapper->SetInputConnection(arrow_glyph->GetOutputPort());
    arrow_glyph_poly_data_mapper->SetLookupTable(lut);
    arrow_glyph_poly_data_mapper->SetScalarRange(hmin, hmax);
    arrow_glyph_poly_data_mapper->Update();

    auto arrow_actor = vtkSmartPointer<vtkActor>::New();
    arrow_actor->SetMapper(arrow_glyph_poly_data_mapper);

    return arrow_actor;
}

vtkSmartPointer<vtkLookupTable> VectorFieldImage::build_lut(
    double hmin, double hmax, size_t nlut
) {
    auto lut = vtkSmartPointer<vtkLookupTable>::New();

    vtkSmartPointer<vtkColorTransferFunction> ctf = 
        vtkSmartPointer<vtkColorTransferFunction>::New();

    double ds = fabs(hmax - hmin)/(double)nlut;

    lut->SetNumberOfTableValues(nlut);

    ctf->AddRGBSegment(
            (hmin)         , 0.0, 0.0, 1.0, 
            (hmin+hmax)/2.0, 1.0, 1.0, 1.0);
    ctf->AddRGBSegment(
            (hmin+hmax)/2.0, 1.0, 1.0, 1.0,
            (hmax)         , 1.0, 0.0, 0.0);

    for (size_t i = 0; i < nlut; ++i) {
        double s    = hmin + ds*(double)i;

        double *rgb = ctf->GetColor(s);

        lut->SetTableValue(i, rgb[0], rgb[1], rgb[2], 1.0);
    }

    return lut;
}

/*
void setArrowLut(vtkSmartPointer<vtkLookupTable> arrowLut)
{
mArrowGlyphPolyDataMapper->SetLookupTable(arrowLut);
mArrowGlyphPolyDataMapper->SetScalarRange(mHmin, mHmax);
mArrowGlyphPolyDataMapper->Update();


void VectorField::setGeometry()
{
  mGeometryDataMapper = vtkSmartPointer<vtkDataSetMapper>::New();
  mGeometryDataMapper->SetInputData(mUGrid);
  mGeometryDataMapper->ScalarVisibilityOff();

  mGeometryActor = vtkSmartPointer<vtkActor>::New();
  mGeometryActor->SetMapper(mGeometryDataMapper);

  mGeometryActor->GetProperty()->SetRepresentationToWireframe();
  mGeometryActor->GetProperty()->SetAmbient(1.0);
  mGeometryActor->GetProperty()->SetDiffuse(0.0);
  mGeometryActor->GetProperty()->SetSpecular(0.0);
}


void VectorField::setIsosurface() 
{
  double h = (mHmin+mHmax)/2;
  DEBUG("Isosurface helicity: " << h);
  mIsosurface = vtkSmartPointer<vtkContourGrid>::New();
  mIsosurface->SetInputData(mUGrid);
  mIsosurface->SetValue(0, h);

  mIsosurfacePolyDataMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  mIsosurfacePolyDataMapper->ScalarVisibilityOff();
  mIsosurfacePolyDataMapper->SetInputData(mIsosurface->GetOutput());
  mIsosurfacePolyDataMapper->Update();

  mIsosurfaceActor = vtkSmartPointer<vtkActor>::New();
  mIsosurfaceActor->SetMapper(mIsosurfacePolyDataMapper);
  mIsosurfaceActor->GetProperty()->SetColor(1.0, 1.0, 1.0);
  mIsosurfaceActor->Modified();
}
*/

/*
}
*/
