/**
 * \file   ReadTecplot.cpp
 * \author L. Nagy
 * 
 * MIT License
 *
 * Copyright (c) [2016] Lesleis Nagy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include "ReadTecplot.h"

std::pair<Mesh, VectorField> ReadTecplot::operator () (
  const std::string & file_name
) {
  using std::cout;
  using std::endl;
  using std::getline;
  using std::ifstream;
  using std::istringstream;
  using std::make_pair;
  using std::string;

  using boost::is_any_of;
  using boost::split;
  using boost::token_compress_on;
  using boost::trim;

  using SplitVector = std::vector<std::string>;

  ifstream fin(file_name);

  Mesh mesh;
  VectorField field;

  string line;
  UInt line_no = 1;
  if (fin.is_open()) {
    while (getline(fin, line)) {

      trim(line);
      istringstream iss(line);

      switch (line_no) {
        case 1: { 
          break;
        }
        case 2: {
          break;
        }
        case 3: {
          break;
        }
        default: {
          SplitVector splt;
          split(splt, line, is_any_of(" "), token_compress_on);

          switch (splt.size()) {
            case 6: {
              istringstream iss_x(splt[0]);
              istringstream iss_y(splt[1]);
              istringstream iss_z(splt[2]);
              istringstream iss_fx(splt[3]);
              istringstream iss_fy(splt[4]);
              istringstream iss_fz(splt[5]);
              
              Float x, y, z, fx, fy, fz;

              iss_x >> x; iss_y >> y; iss_z >> z;
              iss_fx >> fx; iss_fy >> fy; iss_fz >> fz;

              mesh.insert_vertex(x, y, z);
              field.insert_vector(fx, fy, fz);
              break;
            }
            case 4 : {
              istringstream iss_n0(splt[0]);
              istringstream iss_n1(splt[1]);
              istringstream iss_n2(splt[2]);
              istringstream iss_n3(splt[3]);

              Index n0, n1, n2, n3;

              iss_n0 >> n0; iss_n1 >> n1; iss_n2 >> n2; iss_n3 >> n3;

              mesh.insert_element(n0, n1, n2, n3, true);
              break;
            }
          }
        }
      }

      line_no = line_no + 1;
    }
  } else {
    throw ReadTecplotException("File '" + file_name + "' could not be found");
  }

  return make_pair(mesh, field);
}
