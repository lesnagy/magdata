/**
 * \file   ParseStdout.cpp
 * \author L. Nagy
 * 
 * MIT License
 *
 * Copyright (c) [2016] Lesleis Nagy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include "ParseStdout.h"

ParseStdout::SummaryTable ParseStdout::operator () (
  const std::string & file_name
) {
  using std::ifstream;
  using std::istringstream;
  using std::string;

  using boost::smatch;
  using boost::regex_search;

  SummaryTable summary_tbl;

  ifstream fin(file_name);

  string line;
  int current_restart = -1;
  if (fin.is_open()) {
    while (getline(fin, line)) {
      smatch match_restart;
      if (regex_search(line, match_restart, *m_regex_restart)) {
        current_restart += 1;
        summary_tbl.push_back({current_restart, NAN, false, NAN, 0, NAN, NAN, NAN});
      }

      smatch match_typical_energy;
      if (regex_search(line, match_typical_energy, *m_regex_typical_energy)) {
        istringstream iss_typical_energy(match_typical_energy[1]);
        iss_typical_energy >> summary_tbl[current_restart].typical_energy;
      }

      smatch match_converge_status;
      if (regex_search(line, match_converge_status, *m_regex_converge_status)) {
        summary_tbl[current_restart].is_failed = true;
      }

      smatch match_delta_f;
      if (regex_search(line, match_delta_f, *m_regex_delta_f)) {
        istringstream iss_delta_f(match_delta_f[1]);
        iss_delta_f >> summary_tbl[current_restart].delta_f;
      }

      smatch match_energy_calls;
      if (regex_search(line, match_energy_calls, *m_regex_energy_calls)) {
        istringstream iss_energy_calls(match_energy_calls[1]);
        iss_energy_calls >> summary_tbl[current_restart].energy_calls;
      }

      smatch match_final_energy;
      if (regex_search(line, match_final_energy, *m_regex_final_energy)) {
        istringstream iss_final_energy(match_final_energy[1]);
        iss_final_energy >> summary_tbl[current_restart].final_energy;
      }

      smatch match_delta_f_tol;
      if (regex_search(line, match_delta_f_tol, *m_regex_delta_f_tol)) {
        istringstream iss_delta_f_tol(match_delta_f_tol[1]);
        iss_delta_f_tol >> summary_tbl[current_restart].delta_f_tol;
      }

      smatch match_sqrt_grad_tol;
      if (regex_search(line, match_sqrt_grad_tol, *m_regex_sqrt_grad_tol)) {
        istringstream iss_sqrt_grad_tol(match_sqrt_grad_tol[1]);
        iss_sqrt_grad_tol >> summary_tbl[current_restart].sqrt_grad_gtol;
      }
    }
  }

  return summary_tbl;
}