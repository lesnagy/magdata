/**
 * \file   Integrate.cpp
 * \author L. Nagy
 * 
 * MIT License
 *
 * Copyright (c) [2016] Lesleis Nagy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include "Integrate.h"

Float Integrate::operator () (
  const        Mesh & mesh, 
  const ScalarField & sf
) const {
  const Mesh::VertexList & verts = mesh.vertices();
  const Mesh::ElementList & elems = mesh.elements();
  const ScalarField::ScalarList & scals = sf.scalars();

  Float s = 0.0;

  for (auto elem : elems) {
    Float x1 = verts[elem[0]][0];
    Float y1 = verts[elem[0]][1];
    Float z1 = verts[elem[0]][2];
    Float s1 = scals[elem[0]];

    Float x2 = verts[elem[1]][0];
    Float y2 = verts[elem[1]][1];
    Float z2 = verts[elem[1]][2];
    Float s2 = scals[elem[1]];

    Float x3 = verts[elem[2]][0];
    Float y3 = verts[elem[2]][1];
    Float z3 = verts[elem[2]][2];
    Float s3 = scals[elem[2]];

    Float x4 = verts[elem[3]][0];
    Float y4 = verts[elem[3]][1];
    Float z4 = verts[elem[3]][2];
    Float s4 = scals[elem[3]];

    Float is = integrate (
      x1, y1, z1, s1,
      x2, y2, z2, s2,
      x3, y3, z3, s3, 
      x4, y4, z4, s4
    );

    s = s + is;
  }

  return s;
}

VectorField::Vector Integrate::operator () (
  const        Mesh & mesh, 
  const VectorField & vf
) const {
  const Mesh::VertexList & verts = mesh.vertices();
  const Mesh::ElementList & elems = mesh.elements();
  const VectorField::VectorList & vects = vf.vectors();

  Float vx = 0.0;
  Float vy = 0.0;
  Float vz = 0.0;

  for (auto elem : elems) {
    Float x1  = verts[elem[0]][0];
    Float y1  = verts[elem[0]][1];
    Float z1  = verts[elem[0]][2];
    Float vx1 = vects[elem[0]][0];
    Float vy1 = vects[elem[0]][1];
    Float vz1 = vects[elem[0]][2];

    Float x2  = verts[elem[1]][0];
    Float y2  = verts[elem[1]][1];
    Float z2  = verts[elem[1]][2];
    Float vx2 = vects[elem[1]][0];
    Float vy2 = vects[elem[1]][1];
    Float vz2 = vects[elem[1]][2];


    Float x3  = verts[elem[2]][0];
    Float y3  = verts[elem[2]][1];
    Float z3  = verts[elem[2]][2];
    Float vx3 = vects[elem[2]][0];
    Float vy3 = vects[elem[2]][1];
    Float vz3 = vects[elem[2]][2];

    Float x4  = verts[elem[3]][0];
    Float y4  = verts[elem[3]][1];
    Float z4  = verts[elem[3]][2];
    Float vx4 = vects[elem[3]][0];
    Float vy4 = vects[elem[3]][1];
    Float vz4 = vects[elem[3]][2];

    Float ivx = integrate (
      x1, y1, z1, vx1,
      x2, y2, z2, vx2,
      x3, y3, z3, vx3, 
      x4, y4, z4, vx4
    );

    Float ivy = integrate (
      x1, y1, z1, vy1,
      x2, y2, z2, vy2,
      x3, y3, z3, vy3, 
      x4, y4, z4, vy4
    );

    Float ivz = integrate (
      x1, y1, z1, vz1,
      x2, y2, z2, vz2,
      x3, y3, z3, vz3, 
      x4, y4, z4, vz4
    );

    vx = vx + ivx;
    vy = vy + ivy;
    vz = vz + ivz;
  }

  VectorField::Vector v = {vx, vy, vz};
  return v;
}

Float Integrate::integrate (
  Float x1, Float y1, Float z1, Float s1,
  Float x2, Float y2, Float z2, Float s2,
  Float x3, Float y3, Float z3, Float s3,
  Float x4, Float y4, Float z4, Float s4
) const {
  return ((s1 + s2 + s3 + s4)*fabs(
    -(x2*y3*z1) + x2*y4*z1 + x1*y3*z2 - x1*y4*z2 + x2*y1*z3 - 
       x1*y2*z3 + x1*y4*z3 - x2*y4*z3 + 
       x4*(-(y2*z1) + y3*z1 + y1*z2 - y3*z2 - y1*z3 + y2*z3) + 
       (-(x2*y1) + x1*y2 - x1*y3 + x2*y3)*z4 + 
       x3*(-(y4*z1) - y1*z2 + y4*z2 + y2*(z1 - z4) + y1*z4)))/24;
}
