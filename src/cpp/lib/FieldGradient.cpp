/**
 * \file   FieldGradient.cpp
 * \author L. Nagy
 * 
 * MIT License
 *
 * Copyright (c) [2016] Lesleis Nagy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include "FieldGradient.h"

std::pair<VectorField, ScalarField> FieldGradient::operator () (
  const        Mesh & mesh, 
  const VectorField & vf
) {

  using std::make_pair;

  vtkSmartPointer<vtkUnstructuredGrid> ugrid = 
    vtkSmartPointer<vtkUnstructuredGrid>::New();

  vtkSmartPointer<vtkDoubleArray> f = 
    vtkSmartPointer<vtkDoubleArray>::New();
  
  vtkSmartPointer<vtkPoints> points = 
    vtkSmartPointer<vtkPoints>::New();

  vtkSmartPointer<vtkGradientFilter> vorticity =
    vtkSmartPointer<vtkGradientFilter>::New();

  vtkSmartPointer<vtkArrayCalculator> helicity =
    vtkSmartPointer<vtkArrayCalculator>::New();

  const Mesh::VertexList & verts = mesh.vertices();
  const Mesh::ElementList & elems = mesh.elements();
  const VectorField::VectorList & vects = vf.vectors();

  // Mesh vertices.
  points->SetNumberOfPoints(verts.size());
  for (Index i = 0; i < verts.size(); ++i) {
    points->SetPoint(i, verts[i][0], verts[i][1], verts[i][2]);
  }
  ugrid->SetPoints(points);

  // Mesh elements.
  for (Index i = 0; i < elems.size(); ++i) {
    vtkIdType element[4] = {
        (long long) elems[i][0], 
        (long long) elems[i][1], 
        (long long) elems[i][2], 
        (long long) elems[i][3]
    };
    ugrid->InsertNextCell(VTK_TETRA, 4, element);
  }

  // Vector field.
  f->SetName("F");
  f->SetNumberOfComponents(3);
  f->SetNumberOfTuples(vects.size());

  for (Index i = 0; i < vects.size(); ++i) {
    double fd[3] = {
        (double)vects[i][0], 
        (double)vects[i][1], 
        (double)vects[i][2]
    };
    f->SetTuple(i, fd);
  }
  ugrid->GetPointData()->AddArray(f);

  // Vorticity field.
  vorticity->ComputeVorticityOn();
  vorticity->SetInputArrayToProcess(0, 0, 0, 0, "F");
  vorticity->SetVorticityArrayName("V");
  vorticity->SetInputData(ugrid);
  vorticity->Update();

  // Helicity field.
  helicity->SetAttributeType(vtkDataObject::POINT);
  helicity->AddVectorArrayName("F");
  helicity->AddVectorArrayName("V");
  helicity->SetResultArrayName("H");
  helicity->SetFunction("F.V");
  helicity->SetInputData(vorticity->GetOutput());
  helicity->Update();

  // Vorticity field output;
  VectorField v_out;
  vtkUnstructuredGrid * vug = 
    vtkUnstructuredGrid::SafeDownCast(vorticity->GetOutput());
  vtkSmartPointer<vtkDoubleArray> vd = 
    vtkDoubleArray::SafeDownCast(vug->GetPointData()->GetArray("V"));
  for (Index i = 0; i < (Size)vd->GetNumberOfTuples(); ++i) {
    v_out.insert_vector (
      vd->GetTuple(i)[0], 
      vd->GetTuple(i)[1], 
      vd->GetTuple(i)[2]
    );
  }

  // Helicity field output.
  ScalarField s_out;
  vtkUnstructuredGrid * hug =
    vtkUnstructuredGrid::SafeDownCast(helicity->GetOutput());
  vtkSmartPointer<vtkDoubleArray> hd =
    vtkDoubleArray::SafeDownCast(hug->GetPointData()->GetArray("H"));
  for (Index i = 0; i < (Size)hd->GetNumberOfTuples(); ++i) {
    s_out.insert_scalar(hd->GetTuple(i)[0]);
  }
  
  return make_pair(v_out, s_out);
}
