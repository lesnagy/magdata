/**
 * \file   Energies.cpp
 * \author L. Nagy
 * 
 * MIT License
 *
 * Copyright (c) [2016] Lesleis Nagy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include "Energies.h"


ModelEnergies Energies::operator() (
    const std::string & root,
    const std::string & material,
    const std::string & geometry,
    unsigned int size,
    unsigned int temperature,
    unsigned int index,
    const std::string & uuid_value
) {
    using std::string;
    using std::vector;

    using boost::filesystem::remove;

    string script_file = generate_script(
        root, material, geometry, size, temperature, index, uuid_value
    );
    
    string command = m_merrill_exe + " " + script_file;
    vector<string> result = exec(command);

    ModelEnergies model_energies = parse(result);

    // Delete the file
    //remove(script_file);

    return model_energies;
}

void Energies::setup_regexs()
{
    using std::make_shared;
    using std::string;

    using boost::regex;


    string str_float(
        "[+-]?(?=[.]?[0-9])[0-9]*(?:[.][0-9]*)?(?:[Ee][+-]?[0-9]+)?"
    );
    
    m_regex_energy_joules = make_shared<regex> (
        "^\\s*Energies in units of J:\\s*$"
    );

    m_regex_anis_energy = make_shared<regex> (
        "^\\s*E-Anis\\s*(" + str_float + ")\\s*$"
    );

    m_regex_ext_energy = make_shared<regex> (
        "^\\s*E-Ext\\s*(" + str_float + ")\\s*$"
    );

    m_regex_demag_energy = make_shared<regex> (
        "^\\s*E-Demag\\s*(" + str_float + ")\\s*$"
    );

    m_regex_exch1_energy = make_shared<regex> (
        "^\\s*E-Exch\\s*(" + str_float + ")\\s*$"
    );

    m_regex_exch2_energy = make_shared<regex> (
        "^\\s*E-Exch2\\s*(" + str_float + ")\\s*$"
    );

    m_regex_exch3_energy = make_shared<regex> (
        "^\\s*E-Exch3\\s*(" + str_float + ")\\s*$"
    );

    m_regex_exch4_energy = make_shared<regex> (
        "^\\s*E-Exch4\\s*(" + str_float + ")\\s*$"
    );

    m_regex_tot_energy = make_shared<regex> (
        "^\\s*E-Tot\\s*(" + str_float + ")\\s*$"
    );
}

std::vector<std::string> Energies::split(const std::string &s, char delim) 
{
    using std::getline;
    using std::move;
    using std::string;
    using std::stringstream;
    using std::vector;

    using boost::smatch;

    stringstream ss(s);
    string item;
    vector<string> elems;
    while (getline(ss, item, delim)) {
      elems.push_back(move(item));
    }
    return elems;
}

std::string Energies::generate_script(
    const std::string & root,
    const std::string & material,
    const std::string & geometry,
    unsigned int size,
    unsigned int temperature,
    unsigned int index,
    const std::string & uuid_value
) {
    using std::endl;
    using std::ofstream;
    using std::string;

    using boost::uuids::uuid;
    using boost::uuids::string_generator;
    using boost::lexical_cast;

    std::string script_name; 
    if (uuid_value == "") {
      uuid u;
      script_name = lexical_cast<std::string>(u) + ".merrill"; 
    } else {
      string_generator gen;
      uuid u = gen(uuid_value);
      script_name = lexical_cast<std::string>(u) + ".merrill"; 
    }

    string dat_file_name = mgsti_file(
        root, material, geometry, size, temperature, index, DAT
    );

    string mesh_file_name = mesh_file(
        root, material, geometry, size
    );

    ofstream fout(script_name);
    if (fout.is_open()) {
        fout << "Set MaxMeshNumber 1" << endl
             << endl
             << "ReadMesh 1 " << mesh_file_name << endl
             << endl
             << "ConjugateGradient" << endl
             << "Set ExchangeCalculator 1" << endl
             << endl
             << material << " " << temperature << " C" << endl
             << "External Field Strength 0.0 mT" << endl
             << "External Field Direction -1.0 -1.0 -1.0" << endl
             << endl
             << "ReadMagnetization " << dat_file_name  << endl
             << endl
             << "reportenergy" << endl
             << endl
             << "End" << endl;
    }

    return script_name;
}

ModelEnergies Energies::parse(const std::vector<std::string> & lines)
{
    using std::isnan;
    using std::istringstream;
    using std::string;
    using std::vector;

    using boost::smatch;
    using boost::regex_search;

    //vector<string> lines = split(stdout_lines, '\n');

    ModelEnergies model_energies;

    bool parse_energy = false;
    for (const string & line : lines) {
        smatch match_energy_joules;
        if (regex_search(line, match_energy_joules, *m_regex_energy_joules)) {
            parse_energy = true;
            continue;
        }

        smatch match_anis_energy;
        if (regex_search(line, match_anis_energy, *m_regex_anis_energy) && parse_energy) {
            istringstream iss_anis_energy(match_anis_energy[1]);
            iss_anis_energy >> model_energies.anis;
        }

        smatch match_ext_energy;
        if (regex_search(line, match_ext_energy, *m_regex_ext_energy) && parse_energy) {
            istringstream iss_ext_energy(match_ext_energy[1]);
            iss_ext_energy >> model_energies.ext;
        }

        smatch match_demag_energy;
        if (regex_search(line, match_demag_energy, *m_regex_demag_energy) && parse_energy) {
            istringstream iss_demag_energy(match_demag_energy[1]);
            iss_demag_energy >> model_energies.demag;
        }

        smatch match_exch1_energy;
        if (regex_search(line, match_exch1_energy, *m_regex_exch1_energy) && parse_energy) {
            istringstream iss_exch1_energy(match_exch1_energy[1]);
            iss_exch1_energy >> model_energies.exch1;
        }

        smatch match_exch2_energy;
        if (regex_search(line, match_exch2_energy, *m_regex_exch2_energy) && parse_energy) {
            istringstream iss_exch2_energy(match_exch2_energy[1]);
            iss_exch2_energy >> model_energies.exch2;
        }

        smatch match_exch3_energy;
        if (regex_search(line, match_exch3_energy, *m_regex_exch3_energy) && parse_energy) {
            istringstream iss_exch3_energy(match_exch3_energy[1]);
            iss_exch3_energy >> model_energies.exch3;
        }

        smatch match_exch4_energy;
        if (regex_search(line, match_exch4_energy, *m_regex_exch4_energy) && parse_energy) {
            istringstream iss_exch4_energy(match_exch4_energy[1]);
            iss_exch4_energy >> model_energies.exch4;
        }

        smatch match_tot_energy;
        if (regex_search(line, match_tot_energy, *m_regex_tot_energy) && parse_energy) {
            istringstream iss_tot_energy(match_tot_energy[1]);
            iss_tot_energy >> model_energies.tot;
        }
    }

    return model_energies;
}


std::vector<std::string> Energies::exec(const std::string & cmd)
{
  std::array<char, 1024> buffer;
  std::string result;
  std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd.c_str(), "r"), pclose);
  if (!pipe) throw std::runtime_error("popen() failed!");
  while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
    result += buffer.data();
  }

  return split(result, '\n'); 
}

