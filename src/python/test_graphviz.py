#!/usr/bin/env python

from graphviz import Graph

graph = Graph('cluster', format='pdf', node_attr={'shape': 'box'}, graph_attr={'pad': '0.5', 'nodesep': '1', 'ranksep': '2'})

graph.node('alice', image='/Users/les/Data/MMDatabase/magnetite/cubo005/lems/90nm/100C/0001_comp.png', label='', penwidth="5")
graph.node('bob', image='/Users/les/Data/MMDatabase/magnetite/cubo005/lems/90nm/100C/0002_comp.png', label='', penwidth="5")

graph.node('cluster', fontsize="80", label="mx: {:20.15e}\nmy: {:20.15e}\nmz: {:20.15e}\n\nmx: {:20.15e}\nmy: {:20.15e}\nmz: {:20.15e}".format(20.34343, 23.2232, 10332.443, 2034.232, 112.222, 12123.0202), penwidth="3")

graph.edge('cluster', 'alice', penwidth="3")
graph.edge('cluster', 'bob', penwidth="3")

graph.render('cluster.gv', view=True)

