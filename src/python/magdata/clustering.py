r'''
The clustering module contains classes and functions to help form data clusters
from a given set of micromagetic models. These sets are the models indexed
within an mgst-tuple.

.. uml::

'''

from argparse import ArgumentParser, RawTextHelpFormatter

from math import sqrt, atan2, acos, pi

from operator import itemgetter, attrgetter

from sklearn import metrics
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import DBSCAN
from sklearn.datasets import make_blobs
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import StandardScaler

from textwrap import dedent

import json

import numpy as np

import os, sys

import pprint

import scipy.cluster.hierarchy as sch
import scipy.spatial.distance as ssd

import statistics

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib import cm

from magdata.list_range_option import list_range
from magdata.global_vars import ROOT_DIR
from magdata.dir_tree_utils import *
from magdata.util_summary_stats import *

from material_parameters import *


def norm(x, y, z):
    return sqrt(x*x + y*y + z*z)



def normalize(x, y, z):
    l = norm(x, y, z)
    return (x/l, y/l, z/l)



def cart_to_sphere(x, y, z, degrees=True):
    R2D = 180.0/pi if degrees is True else 1.0

    nmag = [normalize(m[0], m[1], m[2]) for m in zip(x, y, z)]

    return [(
            norm(m[0], m[1], m[2]),
            atan2(m[1], m[0])*R2D,
            acos(m[2])*R2D
        ) for m in nmag
    ]



class MMDisplayable:
    r'''
    A displayable class is one that can convert its internal representation
    in to a string, it has no data of its own.
    '''
    def __init__(self):
        pass



    def __str__(self):
        r'''
        Produce a string representation of this object.
        '''
        fld_max = self._fld_maxl()

        frmt_members = []
        for k, v in self.__dict__.items():
            frmt_members.append(
                '{{:{}s}} : {}'.format(
                    fld_max, self._frmt(v)
                ).format(k, self._nstr(v))
            )

        return '\n'.join(frmt_members)



    def _frmt(self, value):
        r'''
        Produce a format string based on this object's internal representation.
        '''
        if type(value) is str:
            return '{:20s}'
        elif type(value) is float:
            return '{:20.15e}'
        elif type(value) is int:
            return '{:8d}'
        elif value is None:
            return '{:20s}'
        else:
            raise ValueError('Unknown format {}'.format(type(value)))



    def _nstr(self, value):
        r'''
        If value is a none, then return a suitable string representation.
        '''
        if value is None:
            return 'None'
        else:
            return value



    def _fld_maxl(self):
        r'''
        Return the maximum length of the fields of this object.
        '''
        return max([len(k) for k in self.__dict__.keys()])



class MMQuantRecord(MMDisplayable):
    r'''
    MMQuantRecord object holds a set of quantities associated with a
    micromagnetic solution. It may be initialized with a dictionary with key
    names being the same as member data variable names. The names of member
    data variabes are as follows:
        * index, the index integer of the micromagnetic structure
        * material, the name of the material
        * geometry, the name of the geometry
        * size, the size of the geometry (in nm ESVD)
        * temperature, the temperature at which the model was run
        * volume, the total volume of the model
        * esvd, the equivalent spherical volume diameter of the model (ESVD)
        * status, the status of the model (SUCCESS or FAILURE)
        * M_tot_x, x component of the total magnetization (integral)
        * M_tot_y, y component of the total magnetization (integral)
        * M_tot_z, z component of the total magnetization (integral)
        * V_tot_x, x component of the total vorticity (integral)
        * V_tot_y, y component of the total vorticity (integral)
        * V_tot_z, z component of the total vorticity (integral)
        * H_tot, the total helicity (integral)
        * anis, the anisotropy energy
        * ext, the external energy
        * demag, the demagneizing field energy
        * exch1, the first exchange energy calculation
        * exch2, the second exchange energy calculation
        * exch3, the third exchange energy calculation
        * exch4, the fourth exchang energy calculation
        * tot, the total energy
        * direction, the direction metric (anisotropy energy divided by anisotrpy constant)
    '''
    def __init__(self, values=None):
        self.index = -1
        self.material = ""
        self.geometry = ""
        self.size = 0
        self.temperature = 0
        self.volume = 0.0
        self.esvd = 0.0
        self.status = ""
        self.M_tot_x = 0.0
        self.M_tot_y = 0.0
        self.M_tot_z = 0.0
        self.V_tot_x = 0.0
        self.V_tot_y = 0.0
        self.V_tot_z = 0.0
        self.H_tot = 0.0
        self.anis = 0.0
        self.ext = 0.0
        self.demag = 0.0
        self.exch1 = 0.0
        self.exch2 = 0.0
        self.exch3 = 0.0
        self.exch4 = 0.0
        self.tot = 0.0
        self.direction = 0.0

        if values is not None:
            self.load_from_dict(values)

            Ms = saturation_magnetization(float(self.temperature), material='magnetite')
            K1 = anisotropy_constant(float(self.temperature), material='magnetite')
            if self.volume > 0.0:
                #print("Vol: {}".format(self.volume))
                self.direction = self.anis / (K1*self.volume)
            else:
                #print("Vol: {}, {}".format(self.volume, self.status))
                pass



    def load_from_dict(self, values):
        r'''
        Populate this object from a dictionary of values (see above for key
        names).
        '''
        self.__dict__.update(values)



    def has_data(self, name):
        r'''
        Test whether this object has a member called 'name'.
        '''
        if name in self.__dict__.keys():
            return True
        else:
            return False



class MMAggregate(MMDisplayable):
    r'''
    MMDisplayable object contains aggregated data for a set of MMQuantRecords.
    The subclasses of MMAggregate indicate the specific aggregation function
    used.
    '''
    def __init__(self):
        self.index = ''
        self.M_tot_x = 0.0
        self.M_tot_y = 0.0
        self.M_tot_z = 0.0
        self.V_tot_x = 0.0
        self.V_tot_y = 0.0
        self.V_tot_z = 0.0
        self.H_tot = 0.0
        self.anis = 0.0
        self.ext = 0.0
        self.demag = 0.0
        self.exch1 = 0.0
        self.exch2 = 0.0
        self.exch3 = 0.0
        self.exch4 = 0.0
        self.tot = 0.0
        self.direction = 0.0



    def agg_fun(self, x):
        r'''
        The aggregation function used to aggregate the data.

        :param x: a list of values to aggregate.
        '''
        return statistics.mean(x)



    def calculate(self, qlist):
        r'''
        Calculate the MMAggregate member values based on a list of MMQuantRecord
        objects.

        :param qlist: a list of MMQuantRecord objects.
        '''
        lst_index = []
        lst_M_tot_x = []
        lst_M_tot_y = []
        lst_M_tot_z = []
        lst_V_tot_x = []
        lst_V_tot_y = []
        lst_V_tot_z = []
        lst_H_tot = []
        lst_anis = []
        lst_ext = []
        lst_demag = []
        lst_exch1 = []
        lst_exch2 = []
        lst_exch3 = []
        lst_exch4 = []
        lst_tot = []
        lst_direction = []

        for q in qlist:
            lst_index.append(q.index)
            lst_M_tot_x.append(q.M_tot_x)
            lst_M_tot_y.append(q.M_tot_y)
            lst_M_tot_z.append(q.M_tot_z)
            lst_V_tot_x.append(q.V_tot_x)
            lst_V_tot_y.append(q.V_tot_y)
            lst_V_tot_z.append(q.V_tot_z)
            lst_H_tot.append(q.H_tot)
            lst_anis.append(q.anis)
            lst_ext.append(q.ext)
            lst_demag.append(q.demag)
            lst_exch1.append(q.exch1)
            lst_exch2.append(q.exch2)
            lst_exch3.append(q.exch3)
            lst_exch4.append(q.exch4)
            lst_tot.append(q.tot)
            lst_direction.append(q.direction)

        self.index = ", ".join(lst_index)
        self.M_tot_x = self.agg_fun(lst_M_tot_x)
        self.M_tot_y = self.agg_fun(lst_M_tot_y)
        self.M_tot_z = self.agg_fun(lst_M_tot_z)
        self.V_tot_x = self.agg_fun(lst_V_tot_x)
        self.V_tot_y = self.agg_fun(lst_V_tot_y)
        self.V_tot_z = self.agg_fun(lst_V_tot_z)
        self.H_tot = self.agg_fun(lst_H_tot)
        self.anis = self.agg_fun(lst_anis)
        self.ext = self.agg_fun(lst_ext)
        self.demag = self.agg_fun(lst_demag)
        self.exch1 = self.agg_fun(lst_exch1)
        self.exch2 = self.agg_fun(lst_exch2)
        self.exch3 = self.agg_fun(lst_exch3)
        self.exch4 = self.agg_fun(lst_exch4)
        self.tot = self.agg_fun(lst_tot)
        self.direction = self.agg_fun(lst_direction)



class MMAggregateMean(MMAggregate):
    r'''
    MMAggregateMean objects aggregate lists of MMQuantRecord objects using
    the mean.
    '''
    def __init__(self):
        super(MMAggregateMean, self).__init__()



class MMAggregateStdev(MMAggregate):
    r'''
    MMAggregateStdev objects aggregate lists of MMQuantRecord objects using
    the standard deviation.
    '''
    def __init__(self):
        super(MMAggregateStdev, self).__init__()


    def agg_fun(self, x):
        stdev = statistics.stdev(x)
        return statistics.stdev(x)



class MMAggregateMedian(MMAggregate):
    r'''
    MMAggregateMedian objects aggregate lists of MMQuantRecord objects using
    the median.
    '''
    def __init__(self):
        super(MMAggregateMedian, self).__init__()



    def agg_fun(self, x):
        return statistics.median(x)



class MMAggregateMin(MMAggregate):
    r'''
    MMAggregateMin objects aggregate lists of MMQuantRecord objects using
    the minimum value.
    '''
    def __init__(self):
        super(MMAggregateMin, self).__init__()



    def agg_fun(self, x):
        return min(x)



class MMAggregateMax(MMAggregate):
    r'''
    MMAggregateMax objects aggregate lists of MMQuantRecord objects using
    the maximum value.
    '''
    def __init__(self):
        super(MMAggregateMax, self).__init__()



    def agg_fun(self, x):
        return max(x)



class Cluster:
    r'''
    A cluster is a collection of MMQuantRecord objects that have been clustered
    accoring to some method (or no method - i.e. it's just a collection of
    MMQuantRecord objects) along with mean, standard deviation, median, min and
    max aggregate information of the cluster. Any given cluster contains
    sub_clusters (which may be empty) this is a dictionary of Cluster objects
    indexed by a label.
    '''
    def __init__(self, qlist=None):

        self.qlist = None           # A list of MMQuantRecord objects
        self.mean = None            # The mean aggregate of qlist
        self.stdev = None           # The standard deviation aggregate of qlist
        self.median = None          # The median aggregate of qlist
        self.parent_cluster = None  # The parent cluster of this cluster
        self.sub_clusters = {}      # A clustering based on

        if qlist is not None:
            self._assign_member_data(qlist)



    def data_list(self, name):
        r'''
        Retrieve the values in qlist of the given name.
        '''
        data_items = []
        for r in self.qlist:
            if name not in r.__dict__.keys():
                raise ValueError(
                    "The key {} is missing from a record".format(name)
                )
            else:
                data_items.append(r.__dict__[name])

        return data_items



    def _assign_member_data(self, qlist):
        self.qlist = qlist

        self.mean = MMAggregateMean()
        self.stdev = MMAggregateStdev()
        self.median = MMAggregateMedian()

        self.mean.calculate(self.qlist)
        self.stdev.calculate(self.qlist)
        self.median.calculate(self.qlist)



class ClusterScatter:
    def __init__(self, points, sizes=None, colours=None, title=None, x_title=None, y_title=None):
        self.figure = Figure()
        self.canvas = FigureCanvas(self.figure)
        self.axis = self.figure.add_subplot(111)

        self.x = [p[0] for p in points]
        self.y = [p[1] for p in points]

        if sizes is not None and colours is not None:
            self.scatter = self.axis.scatter(self.x, self.y, s=sizes, c=colours)
            self.figure.colorbar(self.scatter)
        elif sizes is not None:
            self.scatter = self.axis.scatter(self.x, self.y, sizes)
        elif colours is not None:
            self.scatter = self.axis.scatter(self.x, self.y, c=colours)

        if title is not None:
            self.axis.set_title(title)
        if x_title is not None:
            self.axis.set_xlabel(x_title)
        if y_title is not None:
            self.axis.set_ylabel(y_title)



    def draw_graphic(self, file_name):
        self.figure.savefig(file_name)



def cluster_by_helicity(cluster, eps=0.1, min_samples=3):

    # Extract helicity
    hels = cluster.data_list('H_tot')

    # Create an array of (Etot, Eanis) pairs
    points = np.array([[h, 1.0] for h in hels])

    # Scale the points
    X = StandardScaler().fit_transform(points)

    # Compute clustering using DBSCAN
    db = DBSCAN(eps=eps, min_samples=min_samples).fit(X)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_

    sizes = [(lambda x : 0.5 if x < 0 else 8)(l) for l in labels]

    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

    if n_clusters_ == 0:
        # If there are no sub_clusters just copy over the existing
        # MMQuantRecords
        cluster.sub_clusters = {0: Cluster(cluster.qlist)}
    else:
        # The unique labels
        unique_labels = set(labels)

        qcluster = {}
        for q,l in zip(cluster.qlist, labels):
            if l >= 0:
                if l not in qcluster.keys():
                    qcluster[l] = []
                qcluster[l].append(q)

        sub_clusters = {}
        for k,v in qcluster.items():
            sub_clusters[k] = Cluster(v)


        cluster.sub_clusters = sub_clusters



def cluster_by_anis_tot_energy(cluster, eps=0.05, min_samples=3):

    # Extract helicity
    anis = cluster.data_list('anis')
    tots = cluster.data_list('tot')

    # Create an array of (Etot, Eanis) pairs
    points = np.array([[E[0], E[1]] for E in zip(tots, anis)])

    # Scale the points
    X = StandardScaler().fit_transform(points)

    # Compute clustering using DBSCAN
    db = DBSCAN(eps=eps, min_samples=min_samples).fit(X)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_

    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

    if n_clusters_ == 0:
        # If there are no sub_clusters just copy opver the existing
        # MMQuantRecords
        cluster.sub_clusters = {0: Cluster(cluster.qlist)}
    else:
        # The unique labels
        unique_labels = set(labels)

        qcluster = {}
        for q,l in zip(cluster.qlist, labels):
            if l >= 0:
                if l not in qcluster.keys():
                    qcluster[l] = []
                qcluster[l].append(q)

        sub_clusters = {}
        for k,v in qcluster.items():
            sub_clusters[k] = Cluster(v)


        cluster.sub_clusters = sub_clusters



def cluster_by_direction_tot_energy(cluster, eps=0.05, min_samples=3):

    # Extract helicity
    direction = cluster.data_list('direction')
    tots = cluster.data_list('tot')

    # Create an array of (Etot, direction) pairs
    points = np.array([[E[0], E[1]] for E in zip(tots, direction)])

    # Scale the points
    X = StandardScaler().fit_transform(points)

    # Compute clustering using DBSCAN
    db = DBSCAN(eps=eps, min_samples=min_samples).fit(X)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_

    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

    if n_clusters_ == 0:
        # If there are no sub_clusters just copy opver the existing
        # MMQuantRecords
        cluster.sub_clusters = {0: Cluster(cluster.qlist)}
    else:
        # The unique labels
        unique_labels = set(labels)

        qcluster = {}
        for q,l in zip(cluster.qlist, labels):
            if l >= 0:
                if l not in qcluster.keys():
                    qcluster[l] = []
                qcluster[l].append(q)

        sub_clusters = {}
        for k,v in qcluster.items():
            sub_clusters[k] = Cluster(v)


        cluster.sub_clusters = sub_clusters



def cluster_by_direction(cluster, eps=5.0, min_samples=2):

    # Extract three cartesian directions
    M_tot_x = cluster.data_list('M_tot_x')
    M_tot_y = cluster.data_list('M_tot_y')
    M_tot_z = cluster.data_list('M_tot_z')

    sphere = np.array(cart_to_sphere(M_tot_x, M_tot_y, M_tot_z))
    print(sphere)
    X = sphere[:,1:]

    # Compute clustering using DBSCAN
    db = DBSCAN(eps=eps, min_samples=min_samples).fit(X)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_

    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

    if n_clusters_ == 0:
        # If there are no sub-clusters just copy over the existing
        # MMQuantRecords
        cluster.sub_clusters = {0: Cluster(cluster.qlist)}
    else:
        # The unique labels
        unique_labels = set(labels)

        qcluster = {}
        for q,l in zip(cluster.qlist, labels):
            if l >= 0:
                if l not in qcluster.keys():
                    qcluster[l] = []
                qcluster[l].append(q)

        sub_clusters = {}
        for k,v in qcluster.items():
            sub_clusters[k] = Cluster(v)


        cluster.sub_clusters = sub_clusters



def cluster_by_success(cluster):

    qcluster = {0 : []}
    for q in cluster.qlist:
        if q.status == 'SUCCESS':
            qcluster[0].append(q)

    sub_clusters = {}
    for k,v in qcluster.items():
        sub_clusters[k] = Cluster(v)

    cluster.sub_clusters = sub_clusters



