import os, sys, re

from magdata.function_decorators import *

from magdata.global_vars import *



@static_var('regex_size', re.compile(r'^(\d+)nm$'))
def available_sizes(root_dir, material, geometry):
    r'''
    Retrieve the available sizes given a material and a geometry.
    '''

    abs_dir = os.path.join(root_dir, material, geometry, 'lems')

    return \
        [int(m.group(1)) for m in
            [available_sizes.regex_size.match(d) for d in
                [f for f in os.listdir(abs_dir)]
            ] if m is not None
        ]



@static_var('regex_temp', re.compile(r'^(\d+)C$'))
def available_temperatures(root_dir, material, geometry, size):
    r'''
    Retrieve the available temperatures given a material, geometry and size
    '''

    abs_dir = os.path.join(
        root_dir,
        material,
        geometry,
        'lems',
        '{}nm'.format(size)
    )

    output = \
        [int(m.group(1)) for m in
            [available_temperatures.regex_temp.match(d) for d in
                [f for f in os.listdir(abs_dir)]
            ] if m is not None
        ]

    output.sort()

    return output



def valid_material_directory(root_dir, material):
    r'''
    Return the material directory if it is valid.
    '''
    directory = os.path.join(root_dir, material)

    if not os.path.isdir(directory):
        print("material: '{}' is invalid".format(material))
        sys.exit(0)

    return directory



def valid_geometry_directory(root_dir, material, geometry):
    directory = valid_material_directory(root_dir, material)

    directory = os.path.join(directory, geometry)

    if not os.path.isdir(directory):
        print("geometry: '{}' is invalid".format(geometry))
        sys.exit(1)

    return directory



def valid_mgst_tuples(root_dir, material, geometry, sizes, temperatures):
    r'''
    build a list of valid mgst tuples (i.e. where the directory
    root_dir/material/geometry/size/temperature exists).
    '''

    all_tuples = []

    for size in sizes:
        if temperatures is not None:
            temps = temperatures
        else:
            temps = available_temperatures(root_dir, material, geometry, size)

        for temperature in temps:
            directory = os.path.join(
                root_dir,
                material,
                geometry,
                'lems',
                '{}nm'.format(size),
                '{}C'.format(temperature)
            )
            if os.path.isdir(directory):
                all_tuples.append({
                    'material': material,
                    'geometry': geometry,
                    'size' : size,
                    'temperature' : temperature
                })

    return all_tuples


def valid_mgsti_tuples(root_dir, material, geometry, size, temperature, sidx=1, eidx=50, didx=1):
    r'''
    Returns a list of mgsti tuples that have tecplot and stdout files
    associated with them.
    '''

    all_tuples = []

    for index in range(sidx, eidx+1, didx):
        tecplot_file = os.path.join (
            root_dir,
            material,
            geometry,
            'lems',
            '{}nm'.format(size),
            '{}C'.format(temperature),
            '{}nm_{}C_mag_{:04d}_mult.tec'.format(size, temperature, index)
        )

        stdout_file = os.path.join (
            root_dir,
            material,
            geometry,
            'lems',
            '{}nm'.format(size),
            '{}C'.format(temperature),
            'stdout_lcl_{}nm_{}C_{:04d}'.format(size, temperature, index)
        )

        if (os.path.isfile(tecplot_file) and os.path.isfile(stdout_file)):
            all_tuples.append (
                {
                    "material": material,
                    "geometry": geometry,
                    "size": size,
                    "temperature": temperature,
                    "index": index
                }
            )

    return all_tuples

