r'''
Some useful utilities when dealing with summary_stats json files.
'''

def success_only(summary_stats):
    return [rec for rec in summary_stats if rec['status'] == 'SUCCESS']



def helicities(summary_stats):
    return [rec['H_tot'] for rec in summary_stats]



def indices(summary_stats):
    return [rec['index'] for rec in summary_stats]



def helicity_range(summary_stats):
    hs = helicities(summary_stats)

    return (min(hs), max(hs))



def m_tot_cart(summary_stats, normalized=False):
    if normalized:
        def normed(mx, my, mz):
            l = sqrt(mx*mx + my*my + mz*mz)
            return (mx/l, my/l, mz/l)

        return [normed(
            rec['M_tot_x'],
            rec['M_tot_y'],
            rec['M_tot_z']
        ) for rec in summary_stats]
    else:
        return [normed(
            rec['M_tot_x'],
            rec['M_tot_y'],
            rec['M_tot_z']
        ) for rec in summary_stats]



def m_tot_sphere(summary_stats, degrees=False):
    mcart = m_tot_cart(summary_stats, normalized=True)




def anis_energies(summary_stats, vol_norm=False):
    if vol_norm:
        return [10E18*rec['anis']/rec['volume'] for rec in summary_stats]
    else:
        return [rec['anis'] for rec in summary_stats]




def tot_energies(summary_stats, vol_norm=False):
    if vol_norm:
        return [10E18*rec['tot']/rec['volume'] for rec in summary_stats]
    else:
        return [rec['tot'] for rec in summary_stats]


