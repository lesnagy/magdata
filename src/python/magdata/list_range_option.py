import re

from argparse import ArgumentTypeError

from magdata.function_decorators import static_var

@static_var("regex_list", re.compile(r'^(\d+)(,\s*\d+)*$'))
@static_var("regex_range", re.compile(r'^(\d+):(\d+):(\d+)$'))
def list_range(val):
    r'''
    Extract a list based on a list string or a range string.
    '''

    match_list = list_range.regex_list.match(val)
    match_range = list_range.regex_range.match(val)
    if match_list or match_range:
        if match_list:
            return [int(x) for x in [x.strip() for x in val.split(',')]]
        elif match_range:
            s = int(match_range.group(1))
            d = int(match_range.group(2))
            e = int(match_range.group(3))
            return list(range(s, e+1, d))

    raise ArgumentTypeError

