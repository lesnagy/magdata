def static_var(varname, value):
    r'''
    Decorate functions so that they provide static variables.
    '''

    def decorate(func):
        setattr(func, varname, value)
        return func
    return decorate

