import re, os, sys

from argparse import ArgumentParser, RawTextHelpFormatter, ArgumentTypeError
from textwrap import dedent

from magdata.global_vars import *
from magdata.list_range_option import *
from magdata.dir_tree_utils import *

parser = ArgumentParser(
    description=dedent(
        r'''
            Generate micromagnetic structure summary data

            LIST  : is a comma separated list
            RANGE : is a string of the form s:d:e where:
                        s -- is the start of the RANGE
                        d -- is the step of the RANGE
                        e -- is the end of the RANGE (inclusive)
                    for example
                        1:1:5 is equivalent to the list 1,2,3,4,5
        '''),
    formatter_class=RawTextHelpFormatter
)

parser.add_argument('--material', type=str, required=True,
    help='the name of the material to process'
)

parser.add_argument('--geometry', type=str, required=True,
    help='the name of the geometry to process'
)

parser.add_argument('--sizes', type=list_range, required=False,
    help='a comma separated LIST or RANGE of sizes'
)

parser.add_argument('--temperatures', type=list_range, required=False,
    help='a comma separated LIST or RANGE of temperatures'
)

parser.add_argument('--root', type=str, default=ROOT_DIR,
    help='the directory database root'
)

args = parser.parse_args()

material = args.material
valid_material_directory(args.root, material)

geometry = args.geometry
valid_geometry_directory(args.root, material, geometry)

sizes = args.sizes
if sizes is None:
    sizes = available_sizes(args.root, material, geometry)
temperatures = args.temperatures

root = args.root
