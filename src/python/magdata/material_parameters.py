#!/usr/bin/env python3

try:
    import matplotlib.pyplot as plt
    import matplotlib.ticker as mtick
    import numpy as np
except ImportError:
    print("**WARNING** material_properties.py cannot display graphs")

from math import *

from optparse import OptionParser


def saturation_magnetization(t, material='iron'):
    if material == 'iron':
        t = 273.0 + t
        return 1.75221e6 - 1.21716e3 * t + 33.3368 * (t ** 2) - 0.363228 * (t ** 3) + 1.96713e-3 * (t ** 4) \
            - 5.98015e-6 * (t ** 5) + 1.06587e-8 * (t ** 6) - 1.1048e-11 * (t ** 7) + 6.16143e-15 * (t ** 8) \
            - 1.42904e-18 * (t ** 9)
    elif material == 'magnetite':
        t_c = 580.0
        return 737.384 * 51.876 * (t_c - t) ** 0.4
    else:
        raise ValueError("Saturation magnetization calulcation with unknown material '{}'".format(material))


def exchange_constant(t, material='iron'):
    if material == 'iron':
        t = 273.0 + t
        return -1.8952e-12 + 3.0657e-13 * t - 1.599e-15 * (t ** 2) + 4.0151e-18 * (t ** 3) - 5.3728e-21 * (t ** 4) \
            + 3.6501e-24 * (t ** 5) - 9.9515e-28 * (t ** 6)
    elif material == 'magnetite':
        t_c = 580.0
        return (sqrt(21622.526 + 816.476 * (t_c - t)) - 147.046) / 408.238e11
    else:
        raise ValueError("Exchange calculation with unknown material '{}'".format(material))


def anisotropy_constant(t, material='iron'):
    if material == 'iron':
        t = (273.0 + t)
        k1 = 54967.1 + 44.2946 * t - 0.426485 * (t ** 2) + 0.000811152 * (t ** 3) - 1.07579e-6 * (t ** 4) + \
            8.83207e-10 * (t ** 5) - 2.90947e-13 * (t ** 6)
        k1 = k1 * (480.0 / 456.0)
        return k1
    elif material == 'magnetite':
        t_c = 580.0
        return -2.13074e-5 * (t_c - t) ** 3.2
    else:
        raise ValueError("Magnetocrystalline anisotropy calculation with unknown material '{}'".format(material))


def saturation_energy(t, material='iron'):
    mu = 1e-7
    ms = saturation_magnetization(t, material=material)

    return 4 * 3.1415926535897932 * mu * ms * ms * 0.5


def lambda_ex(t, material='iron'):
    a = exchange_constant(t, material=material)
    kd = saturation_energy(t, material=material)

    try:
        r_val = sqrt(a / kd)
        return r_val
    except ValueError:
        return None
    except ZeroDivisionError:
        return None


def q_hardness(t, material='iron'):
    k1 = anisotropy_constant(t, material=material)
    kd = saturation_energy(t, material=material)

    try:
        r_val = k1 / kd
        return r_val
    except ValueError:
        return None
    except ZeroDivisionError:
        return None


def main(program_options):
    if not program_options.temperature:
        material = program_options.material

        if program_options.mathematica:
            if program_options.material == 'iron':
                # Mathematica saturation magnetization
                str_Ms = '1.75221e6 - 1.21716e3 * t + 33.3368 * (t ** 2) ' \
                         '- 0.363228 * (t ** 3) + 1.96713e-3 * (t ** 4) ' \
                         '- 5.98015e-6 * (t ** 5) + 1.06587e-8 * (t ** 6) '\
                         '- 1.1048e-11 * (t ** 7) + 6.16143e-15 * (t ** 8) '\
                         '- 1.42904e-18 * (t ** 9)'
                str_Ms = str_Ms.replace('t', '(273.0 + t)')
                str_Ms = str_Ms.replace('**', '^')
                str_Ms = str_Ms.replace('e', '`*^')
                print("Saturation magnetization ({})".format(program_options.material))
                print(str_Ms)
                print("")

                # Mathematica exchange
                str_Ae = '-1.8952e-12 + 3.0657e-13 * t - 1.599e-15 * (t ** 2) '\
                         '+ 4.0151e-18 * (t ** 3) - 5.3728e-21 * (t ** 4) '\
                         '+ 3.6501e-24 * (t ** 5) - 9.9515e-28 * (t ** 6)'
                str_Ae = str_Ae.replace('t', '(273.0 + t)')
                str_Ae = str_Ae.replace('**', '^')
                str_Ae = str_Ae.replace('e', '`*^')
                print("Exchange constant ({})".format(program_options.material))
                print(str_Ae)
                print("")

                # Mathematica magnetocrystalline anisotropy
                str_K = '54967.1 + 44.2946 * t - 0.426485 * (t ** 2) '\
                        '+ 0.000811152 * (t ** 3) - 1.07579e-6 * (t ** 4) + '\
                        '8.83207e-10 * (t ** 5) - 2.90947e-13 * (t ** 6)'
                str_K = str_K.replace('t', '(273.0 + t)')
                str_K = str_K.replace('**', '^')
                str_K = str_K.replace('e', '`*^')
                str_K = '({})*(480.0 / 456.0)'.format(str_K)
                print("Magnetocrystalline anisotropy ({})".format(program_options.material))
                print(str_K)
                print("")

                return
            elif program_options.material == 'magnetite':
                str_Ms = '737.384 * 51.876 * (t_c - t) ** 0.4'
                str_Ms = str_Ms.replace('t_c', '580.0')
                str_Ms = str_Ms.replace('**', '^')
                str_Ms = str_Ms.replace('e', '`*^')
                print("Saturation magnetization ({})".format(program_options.material))
                print(str_Ms)
                print("")

                str_Ae = '(Sqrt[21622.526 + 816.476 * (t_c - t)] - 147.046) / 408.238e11'
                str_Ae = str_Ae.replace('t_c', '580.0')
                str_Ae = str_Ae.replace('**', '^')
                str_Ae = str_Ae.replace('e', '`*^')
                print("Exchange constant ({})".format(program_options.material))
                print(str_Ae)
                print("")

                str_K = '-2.13074e-5 * (t_c - t) ** 3.2'
                str_K = str_K.replace('t_c', '580.0')
                str_K = str_K.replace('**', '^')
                str_K = str_K.replace('e', '`*^')
                print("Magnetocrystalline anisotropy ({})".format(program_options.material))
                print(str_K)
                print("")

                return

        if material == 'iron':
            temperatures = np.arange(20.0, 769, 0.01)
        elif program_options.material == 'magnetite':
            temperatures = np.arange(20.0, 580.0, 0.01)
        else:
            raise ValueError("Unknown material '{}'".format(program_options.material))

        ms = list(map(lambda t: saturation_magnetization(t, material=material), temperatures))
        a = list(map(lambda t: exchange_constant(t, material=material), temperatures))
        k1 = list(map(lambda t: anisotropy_constant(t, material=material), temperatures))
        kd = list(map(lambda t: saturation_energy(t, material=material), temperatures))
        lex = list(map(lambda t: lambda_ex(t, material=material), temperatures))
        qhd = list(map(lambda t: q_hardness(t, material=material), temperatures))

        plt.close('all')

        f, axarr = plt.subplots(3, 2, sharex='col')

        axarr[0][0].plot(temperatures, ms)
        axarr[0][0].set_title('Ms')
        axarr[0][0].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))

        axarr[1][0].plot(temperatures, a)
        axarr[1][0].set_title('A')

        axarr[2][0].plot(temperatures, k1)
        axarr[2][0].set_title('K1')
        axarr[2][0].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))

        axarr[0][1].plot(temperatures, kd)
        axarr[0][1].set_title('Kd')
        axarr[0][1].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))

        axarr[1][1].plot(temperatures, lex)
        axarr[1][1].set_title('Exchange length')

        axarr[2][1].plot(temperatures, qhd)
        axarr[2][1].set_title('q-hardness')

        plt.suptitle('Material parameters for {}'.format(program_options.material))
        plt.show()
    else:
        temperatures = program_options.temperature
        material = program_options.material

        if program_options.material == 'iron':
            if temperatures < 1.0 or temperatures > 770.0:
                print("** WARNING ** temperature out of acceptable interpolation range")
        elif program_options.material == 'magnetite':
            if temperatures < 1.0 or temperatures > 580.0:
                print("** WARNING ** temperature out of acceptable interpolation range")

        ms = saturation_magnetization(temperatures, material=material)
        a = exchange_constant(temperatures, material=material)
        k1 = anisotropy_constant(temperatures, material=material)
        kd = saturation_energy(temperatures, material=material)
        lex = lambda_ex(temperatures, material=material)
        qhd = q_hardness(temperatures, material=material)

        print('Material parameters for {} @ {}C'. format(program_options.material, temperatures))
        print("  Ms         {}".format(ms))
        print("  A          {}".format(a))
        print("  K1         {}".format(k1))
        print("  Kd         {}".format(kd))
        print("  lambda_ex  {}".format(lex))
        print("  q_hardness {}".format(qhd))


def material_parameters(t, material='iron'):
    if material == 'iron':
        anisform = 'cubic'
    elif material == 'magnetite':
        anisform = 'cubic'
    else:
        raise ValueError("Attempting to calculate material parameters for unknown material '{}'".format(material))

    ms = saturation_magnetization(t, material=material)
    a = exchange_constant(t, material=material)
    k1 = anisotropy_constant(t, material=material)
    kd = saturation_energy(t, material=material)
    lex = lambda_ex(t, material=material)
    qhd = q_hardness(t, material=material)

    return {'anisform': anisform,
            'material': material,
            'Aex': a,
            'Ms': ms,
            'K1': k1,
            'Kd': kd,
            'lambda_ex': lex,
            'q_hardness': qhd}


def iron_parameters(t):
    return material_parameters(t, 'iron')


def magnetite_parameters(t):
    return material_parameters(t, 'magnetite')


if __name__ == '__main__':
    parser = OptionParser()

    parser.add_option('-m', '--material', dest='material',
                      help='material for which to calculate parameters',
                      type='string'
                      )
    parser.add_option('-t', '--temperature', dest='temperature',
                      help='temperature at which to calculate parameters, if not give a graph is drawn',
                      type='float',
                      default=None
                      )
    parser.add_option('--mathematica', dest='mathematica', 
                      action='store_true', default=False, 
                      help='get mathematica string for expression'
                      )

    (options, args) = parser.parse_args()

    if not options.material:
        print("\n** ERROR: Parameter 'material' is required **\n")
        parser.print_help()
        exit(1)

    if options.material not in ['iron', 'magnetite']:
        print("\n** ERROR: Unknown material '{}', must be one of: iron, magnetite\n".format(options.material))
        exit(1)

    main(options)
